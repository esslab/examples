<?php

namespace App\Models\Smartpay;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $client_id
 * @property string $guid
 * @property int $credit_sum
 * @property int $credit_period
 * @property int $overdraft_sum
 * @property int $overdraft_period
 * @property int $grace_period
 * @property float $credit_rate
 * @property float $overdraft_rate
 * @property float $over_rate_expired_privilege
 */
class GpbOnlineCredit extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'gpb_online_credit';
    protected $hidden = ['id'];
}