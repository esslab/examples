<?php


namespace App\Models\CRM\Smartpay;

/**
 * Class KassaAccountPayment
 * @property $qr_id Иденификатор QR кода
 * @package App\Models\CRM\Smartpay
 */
class SbpAccountPayment extends AccountPayment
{
    public const QR_TYPE = 'QRDynamic';
    public string $channel = 'sbp';
    public string $description;
    public const PARTNER = 'SBP';
    public const STATUSES = [
        'DECLINED'     => self::CANCEL,
        'SUCCESS'      => self::SUCCESS,
        'IN_PROGRESS'  => self::PROGRESS,
        'NO_INFO'      => self::NO_INFO,
        ''             => self::ERROR,
        'NOT_FOUND'    => self::NOT_FOUND
    ];

    public function __construct($payment = null, array $attributes = [])
    {
        $this->qr_id = $payment['QrId'] ?? null;
        $this->cancel_after = config('smartpay.providers.sbp.cancellation_days');
        parent::__construct($payment, $attributes);
    }

    /**
     * @return string|null
     */
    public function getMerchId(): ?string
    {
        return $this->qr_id;
    }

    /**
     * @param array $payment
     */
    public function fillFromBankPayment(array $payment): void
    {
        $this->bank_status = $payment['paymentStatus'] ?? null;
        $this->order_id = $payment['order'] ?? null;
        $this->amount = $payment['amount'] ?? null;
        $this->trx_id = $payment['transactionId'] ?? null;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->bank_status === self::NOT_FOUND || parent::isExpired();
    }
}