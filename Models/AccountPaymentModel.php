<?php

namespace App\Models\Smartpay;


use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountPaymentModel
 *
 * @package App\Models\Smartpay
 */
class AccountPaymentModel extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'account_payments';
    protected $primaryKey = 'id_transaction';
    public $fillable = ['id_transaction', 'contract', 'status', 'partner', 'login', 'user_id', 'product'];
}