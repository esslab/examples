<?php

namespace App\Models\Smartpay;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $client_id
 * @property int $credit_sum
 * @property string $inn
 */
class TochkaOnlineCredit extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'tochka_online_credit';
    protected $hidden = ['id'];
}