<?php

namespace App\Models\CRM\Smartpay;

/**
 * Class PaymentData
 * @package App\Models\CRM\Smartpay
 * @property string|int $code - Код ответа
 * @property string $description - Описание ответа
 * @property string $merchantTransactionId - Идентификатор транзакции магазина
 * @property string $purchase_short_desc - Краткое описание товара
 * @property string $purchase_long_desc - Полное описание товара
 * @property string $account_id - Номер счета
 * @property string $account_amount - Сумма
 * @property string $account_currency - Валюта платежа
 * @property string $account_exponent - Экспонента валюты
 * @property bool $isRecurrent - Рекуррентный платеж
 * @property string $card_id - Интеграционный идентификатор карты в Банке
 * @property string $responseType - Тип ответа
 * @property string $present - Наличие платежной карты
 * @property string $email - Адрес электронной почты пользователя
 * @property string $contractId - Номер договора
 * @property string $login - Логин пользователя
 */
class GpbCheckPayment extends GpbPayment implements CardPaymentInterface
{
    public const PAYMENT_TYPE = 'payment-avail-response';

    /**
     * @param null $crmPayment
     * @param null $crmResponse
     */
    public function makeAfterPaymentActions($crmResponse, $crmPayment)
    {
        $this->fillFromCrmResponse($crmResponse);
    }
    /**
     * @param array $result
     * @return void
     */
    private function fillFromCrmResponse(array $result): void
    {
        if ($result) {
            $checkPaymentAvailResult = $result['CheckPaymentAvailResult']['OutObject'];
            $this->merchantTransactionId = $result['TransId'];
            $this->code = $checkPaymentAvailResult['Result']['Code'];
            $this->description = html_entity_decode($checkPaymentAvailResult['Result']['Desc']);
            $this->purchase_short_desc = html_entity_decode($checkPaymentAvailResult['Purchase']['ShotDesc']) ?? null;
            $this->purchase_long_desc = html_entity_decode($checkPaymentAvailResult['Purchase']['LongDesc']) ?? null;
            $this->account_amount = $checkPaymentAvailResult['AccountAmount']['Amount'];
            $this->account_currency = $checkPaymentAvailResult['AccountAmount']['Currency'];
            $this->account_exponent = $checkPaymentAvailResult ['AccountAmount']['Exponent'] ?? null;
            $this->card_id = $checkPaymentAvailResult['Card']['CardId'];
            $this->present = $checkPaymentAvailResult['Card']['Present'];
        }
    }
}
