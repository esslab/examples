<?php

namespace App\Models\Smartpay;

use Carbon\Carbon;

/**
 * @property int $user_id
 * @property int $refresh_token
 * @property int $iv
 * @property int $expires_in
 * @property Carbon $updated_at
 */
class SBBOLOauthAccessTokens extends SBBOLTokens
{
    protected $connection = 'pgsql';
    protected $table = 'sbbol_oauth_access_tokens';
    public $fillable = ['user_id', 'access_token', 'refresh_token', 'iv', 'expires_in', 'cert_bank_uuid', 'cert_bank'];
}
