<?php

namespace App\Models\CRM\Smartpay;

use App\Models\CRM\Model;

/**
 * Class PaymentData
 * @package App\Models\CRM\Smartpay
 * @property string|int $code - Код ответа
 * @property string|int $ext_result_code - Расширенный код ошибки
 * @property string $description - Описание ответа
 * @property string $merchantTransactionId - Идентификатор транзакции магазина
 * @property string $purchase_short_desc - Краткое описание товара
 * @property string $purchase_long_desc - Полное описание товара
 * @property string $account_id - Номер счета
 * @property string $accountAmount - Сумма
 * @property string $account_currency - Валюта платежа
 * @property string $account_exponent - Экспонента валюты
 * @property bool $isRecurrent - Рекуррентный платеж
 * @property string $card_id - Интеграционный идентификатор карты в Банке
 * @property string $responseType - Тип ответа
 * @property string $present - Наличие платежной карты
 * @property string $email - Адрес электронной почты пользователя
 * @property string $contractId - Номер договора
 * @property string $login - Логин пользователя
 */
abstract class GpbPayment extends Model
{
    public const DEFAULT_AMOUNT = 100;
    public const PAYMENT_AVAIL_TYPE = 'payment-avail-response';
    public const PAYMENT_SUCCESSFUL_MESSAGE = 'Payment succeeded';
    public const PAYMENT_ALLOWED = 'Payment allowed';
    public const PAYMENT_FAILURE_CODE = 2;
    public const PAYMENT_SUCCESS_CODE = 1;
    public const DEFAULT_EXPONENT = 2;
    public const DEFAULT_CURRENCY_CODE = 643;
    public const BUSINESS_CARD = 'BusinessCard';
    public const FAILED_MESSAGE = 'Возникла ошибка при проведении платежа.';
    public const FAILED_RECURRENT_MESSAGE = 'Указанную карту нельзя использовать для проведения автоплатежей';
    private const FAILED_DESCRIPTION = 'Unable to process the payment';
    private const SUCCEEDED_DESCRIPTION = 'Operation Succeeded';

    /**
     * PaymentData constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->account_exponent = self::DEFAULT_EXPONENT;
        $this->account_currency = self::DEFAULT_CURRENCY_CODE;
        $this->responseType = static::PAYMENT_TYPE;
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        $data = [
            'result' => [
                'code' => $this->code,
                'desc' => $this->description
            ]
        ];

        if (!empty($this->merchantTransactionId)) {
            $data['merchant-trx'] = $this->merchantTransactionId;
            $data['purchase'] = [
                'shortDesc' => $this->purchase_short_desc,
                'longDesc' => $this->purchase_long_desc,
                'account-amount' => [
                    'id' => $this->account_id,
                    'amount' => $this->accountAmount,
                    'fee' => 0,
                    'currency' => $this->account_currency,
                    'exponent' => $this->account_exponent,
                ]
            ];
        }

        if ($this->isRecurrent) {
            $data['RegisterRecurrent'] = 'Y';
            $data['transaction-type'] = 'CardRegister';
        }

        if (!empty($this->card_id)) {
            $data['card'] = [
                'present' => $this->present,
                'ref' => $this->card_id
            ];
        }

        return $data;
    }

    /**
     * @param $value
     * @return int
     */
    public function getCodeAttribute($value): int
    {
        return (int)$value;
    }

    /**
     * @param $paymentService
     */
    public function makePaymentRequest($paymentService): void
    {
        $paymentService->self::PAYMENT_METHOD;
    }

    /**
     * @param \Exception $exception
     */
    public function setFailedResponse(\Exception $exception): void
    {
        $this->description = $exception->getMessage() ?? self::FAILED_DESCRIPTION;
        $this->code = $this::PAYMENT_FAILURE_CODE;
    }

    /**
     *
     */
    public function setSucceededResponse(): void
    {
        $this->description = self::SUCCEEDED_DESCRIPTION;
        $this->code = $this::PAYMENT_SUCCESS_CODE;
    }

    /**
     * @return callable
     */
    public function fillFromCRMPayment(): callable
    {
        /**
         * @param CrmPayment $crmPayment
         */
        return function (CrmPayment $crmPayment) {
            if ($crmPayment->isRecurrent()) {
                $this->setRecurrentProperties();
            } else {
                $this->setPaymentProperties($crmPayment);
            }

            $this->account_id = $this->getAccountId($crmPayment);
            $this->merchantTransactionId = $crmPayment->transaction_id;
        };
    }

    /**
     *
     */
    protected function setRecurrentProperties(): void
    {
        $this->isRecurrent = true;
        $this->purchase_long_desc = $this->purchase_short_desc = 'Recurrent payment';
        $this->accountAmount = self::DEFAULT_AMOUNT;
        $this->code = self::PAYMENT_SUCCESS_CODE;
        $this->description = self::PAYMENT_ALLOWED;
    }

    /**
     * @param CrmPayment $crmPayment
     */
    protected function setPaymentProperties(CrmPayment $crmPayment): void
    {
        $this->accountAmount = $crmPayment->amount;
        $this->email = $crmPayment->email;
        $this->contractId = $crmPayment->contract_id;
        $this->login = $crmPayment->login;
        $this->ext_result_code = $crmPayment->ext_result_code ?? null;
    }

    /**
     * @param $crmPayment
     * @return string
     */
    protected function getAccountId(CrmPayment $crmPayment): string
    {
        return ($crmPayment->card_type === self::BUSINESS_CARD)
            ? config('smartpay.providers.acquiring')['account_id_business']
            : config('smartpay.providers.acquiring')['account_id'];
    }
}
