<?php

namespace App\Models\CRM\Smartpay;

use Carbon\Carbon;
use Carbon\CarbonTimeZone;
use Jenssegers\Model\Model;
use YooKassa\Model\PaymentInterface;

/**
 * Class AccountPayment
 * @package App\Models\CRM\Smartpay
 * @property string const STATUSES - массив статусов партнера
 * @property string $id_transaction - id транзакции магазина
 * @property string $trx_id - id транзакции банка
 * @property string $contract - номер договора или счета
 * @property string $account_number
 * @property string $email - адрес электронной почты
 * @property string $amount - сумма платежа
 * @property string $credit_amount - минимальная сумма платежа
 * @property string $bank_status - статус платежа
 * @property string $currency - валюта
 * @property string $crm_status - статус платежа CRM
 * @property string $client_id - фильтр по id клиента
 * @property string $partner - Yandex | Sberbank
 * @property string $order_id - Номер заказа Sberbank
 * @property string $channel - Канал логирования
 * @property string $product - Тип продуукта для СББОЛ
 * @property Carbon $date_time_transaction - время проведения транзакции
 * @property string|null $product_type - Тип кредитного продукта для Сбер
 */
abstract class AccountPayment extends Model
{
    const FORCE_CANCELLED = 'Force-cancelled';
    const EXPIRED = 'Expired';
    public string $currency = 'RUR';
    public const PENDING = 'Pending';
    public const NOT_FOUND = 'Not found';
    public const SUCCESS = 'Succeeded';
    public const CANCEL = 'Canceled';
    public const PROGRESS = 'In process';
    public const NO_INFO = 'No Info';
    public const ERROR = 'ERROR';
    public const INITIAL_STATUS = self::PENDING;
    public const BANK = '';
    public const AVAILABLE_PARTNERS = ['yandex', 'sberbank', 'SBP'];
    public const PARTNER = 'Not Defined';
    public string $channel;
    public $login;
    public $tz;
    public string $partner;
    /**
     * Отменять платеж авоматически при синхронизации, если прошло CANCEL_AFTER дней
     */
    public const CANCEL_AFTER = 0;
    public int $cancel_after = self::CANCEL_AFTER;

    /**
     * AccountPayment constructor.
     * @param null $payment - CRM payment array
     * @param array $attributes
     */
    public function __construct($payment = null, array $attributes = [])
    {
        $this->login = auth()->user()->login ?? null;
        $this->crm_status = $payment['Status'] ?? self::INITIAL_STATUS;
        $this->partner = static::PARTNER;
        $this->date_time_transaction = !empty($payment['DateTimeTransaction']) ? Carbon::createFromFormat('m/d/Y H:i:s', $payment['DateTimeTransaction']) : Carbon::now();
        $this->tz = new CarbonTimeZone('Europe/Moscow');

        if ($payment) {
            $this->fillFromCRMPayment($payment);
        }

        parent::__construct($attributes);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->bank_status ? static::STATUSES[$this->bank_status] : $this->crm_status;
    }

    /**
     * @return string
     */
    public function getMerchId(): ?string
    {
        return $this->trx_id;
    }

    /**
     * Fill from CRM payment
     * @param array $payment
     */
    public function fillFromCRMPayment($payment): void
    {
        $this->id_transaction = $payment['IdTransaction'] ?? null;
        $this->trx_id = $payment['TRXId'] ?? null;
        $this->contract = $payment['Contract'] ?? '??';
        $this->account_number = $payment['Account'] ?? null;
        $this->amount = $payment['Amount'] ?? null;
        $this->order_id = $payment['OrderId'] ?? null;
    }

    /**
     * @param array $payment
     * @return void
     */
    abstract public function fillFromBankPayment(array $payment): void;

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        $time = clone $this->date_time_transaction;

        return $this->cancel_after && $time->addDays($this->cancel_after) < Carbon::now();
    }
}