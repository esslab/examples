<?php


namespace App\Models\CRM\Smartpay;


/**
 * Class KassaAccountPayment
 * @property string $product_type - тип продукта из ЦРМ
 * @package App\Models\CRM\Smartpay
 */
class SberCreditAccountPayment extends SberAccountPayment
{
    public const PRODUCT_TYPE = ['New VKL', 'Credit', 'VKL'];
    public const PRODUCT = 'kvk';

    public function __construct($request, $contract)
    {
        $this->product_type = $request->product_type;
        $this->product = self::PRODUCT ?? null;

        parent::__construct(null, [
            'credit_product_code' => $request->credit_product_code ?? null,
            'credit_term'         => $request->credit_term ?? null,
            'amount'              => $request->amount,
            'credit_amount'       => $request->credit_amount ?? null,
            'contract'            => $contract->number,
            'order_id'            => $contract->sid . '-' . time(),
        ]);
    }
}