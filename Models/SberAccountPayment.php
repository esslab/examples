<?php


namespace App\Models\CRM\Smartpay;


/**
 * Class KassaAccountPayment
 * @property string $credit_product_code - код кредитнго продукта
 * @property string $credit_term - срок кредита
 * @property string $product - тип продукта
 * @package App\Models\CRM\Smartpay
 */
class SberAccountPayment extends AccountPayment
{
    public const CREDIT_OFFER_PAGE = '/finances/sberbank-credit';
    public const CORCHECKOUT_SUCCESS = '/finances/success';
    public const PAYMENT_PAGE = '/finances';
    public const CORCHECKOUT_ERROR = '/finances/error';
    public const CREDIT_REQUEST_URL = '/ic/dcb/index.html#/credits/credit-financing/credit-partners?order=';
    public const BANK = 'Sberbank';
    public const CRYPTO_SMS = 'SMS';
    public const CRYPTO_TOKEN = 'Token';
    public const CANCEL_AFTER = 14 * 24;
    public const PARTNER = 'Sberbank';
    public string $channel = 'kvk';
    public string $product;
    public string $partner = self::PARTNER;
    public $login;
    public $fillable = ['id_transaction', 'amount', 'contract', 'order_id', 'account_number', 'credit_product_code', 'credit_term', 'bank_status'];
    public const STATUSES = [
        'NOT_FOUND'         => self::NOT_FOUND,
        'ACCEPTED'          => self::PROGRESS,
        'ACCEPTED_BY_ABS'   => self::PROGRESS,
        'CARD2'             => self::CANCEL,
        'CHECKERROR'        => self::PROGRESS,
        'CHECKERROR_BANK'   => self::PROGRESS, // deprecated
        'CREATED'           => self::PROGRESS,
        'DELAYED'           => self::PROGRESS,
        'DELIVERED'         => self::PROGRESS,
        'EXPORTED'          => self::PROGRESS, // deprecated
        'FRAUDALLOW'        => self::PROGRESS,
        'FRAUDDENY'         => self::PROGRESS,
        'FRAUDREVIEW'       => self::PROGRESS,
        'FRAUDSENT'         => self::PROGRESS,
        'FRAUDSMS'          => self::PROGRESS,
        'IMPLEMENTED'       => self::SUCCESS,
        'INVALIDEDS'        => self::CANCEL,
        'PARTSIGNED'        => self::PROGRESS,
        'PROCESSING'        => self::PROGRESS, // deprecated
        'RECALL'            => self::CANCEL,
        'REFUSEDBYABS'      => self::CANCEL,
        'REQUISITEERROR'    => self::CANCEL,
        'SIGNED'            => self::PROGRESS,
        'SUBMITTED'         => self::PROGRESS, // deprecated
        'WORKFLOW_FAULT'    => self::ERROR,
        'REFUSEDBYBANK'     => self::CANCEL,
        'DELIVERED_RZK'     => self::PROGRESS,
        'PROCESSING_RZK'    => self::PROGRESS,
        'REFUSED_BY_RZK'    => self::PROGRESS,
        'RZK_SIGN_ERROR'    => self::PROGRESS,
        'NOT_ACCEPTED_RZK'  => self::PROGRESS,
        'SENDING_TO_RZK'    => self::PROGRESS,
        'TO_PROCESSING_RZK' => self::PROGRESS,
        'DELETED'           => self::CANCEL,
    ];

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        $this->cancel_after = config('smartpay.providers.sbbol.cancellation_days');

        return ($this->getStatus() === self::NOT_FOUND) && parent::isExpired();
    }

    /**
     * @param array $payment
     */
    public function fillFromBankPayment(array $payment): void
    {
        $this->bank_status = $payment['bankStatus'] ?? $payment['cause'] ?? null;
    }
}