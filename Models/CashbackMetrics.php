<?php

namespace App\Models\Smartpay;


use Illuminate\Database\Eloquent\Model;

/**
 * Class CashbackMetrics
 * @package App\Models\Smartpay
 *
 * $target - значяение в литрах для получения кэшбэка
 * $client_id - идентификатор клиента
 *
 */
class CashbackMetrics extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'cashback_metrics';
    protected $primaryKey = 'client_id';
    protected $hidden = ['client_id', 'created_at', 'updated_at'];
}
