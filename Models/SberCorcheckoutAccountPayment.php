<?php


namespace App\Models\CRM\Smartpay;


/**
 * Class KassaAccountPayment
 * @property string $product_type - тип продукта из ЦРМ
 * @package App\Models\CRM\Smartpay
 */
class SberCorcheckoutAccountPayment extends SberAccountPayment
{
    public const PRODUCT_TYPE = ['CCO'];
    public const PAYMENT_URL = '/finances/success';
    public const PAYMENT_FAILED_URL = '/finances/error';
    public const PRODUCT = 'corcheckout';
    public string $type = 'default';
    public ?string $inn;
    public ?string $user_id;

    public function __construct($request, $contract)
    {
        $this->product_type = self::PRODUCT_TYPE[0];
        $this->product = self::PRODUCT;
        $this->type = $request->type ?? 'default';

        parent::__construct(null, [
            'amount'              => $request->amount,
            'contract'            => $contract->number,
            'order_id'            => $contract->sid . '-' . time(),
        ]);
    }
}