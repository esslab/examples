<?php

namespace App\Models\CRM\Smartpay;

use App\Http\Requests\V2\Smartpay\SavePaymentResultRequest;
use Illuminate\Http\Request;

/**
 * Class PaymentResult
 * @package App\Models\CRM\Smartpay
 * @property string $contract_id - Номер договора
 * @property string $account_no - Номер расчетного счета
 * @property string $payment_type - Тип платежа
 * @property string $ptype - Тип платежа
 * @property string $pps_trxid - Идентификатор транзакции в PPS
 * @property string $transaction_id - ID транзакции магазина
 * @property string $trx_datetime - Дата и время транзакции
 * @property string $rrn - RN транзакции
 * @property string $card_id - Интеграционный идентификатор карты в Банке
 * @property string $cardno_masked - Маскированный номер карты
 * @property string $card_expires_at - Срок действия карты
 * @property string $card_saved - Признак сохранения реквизитов карты в банке
 * @property string $amount - Сумма
 * @property string $initiator - Инициатор запроса
 * @property string $p_storage_card_recurrent
 * @property string $currency - Валюта
 * @property string $card_type - Тип банковской карты
 * @property string $result_code - Код результата
 * @property string $ext_result_code - Расширенный код ошибки
 * @property string $result_msg - Текст результата
 * @property string $accept - Признак согласия на списание денежных средств с банковской карты
 * @property string $login - Логин пользователя
 * @property string $email - Email для отправки кассового чека
 * @property string $sumMin - Минимальная сумма при достижении которой происходит автоплатеж
 * @property string $sumMax - Максимальная сумма списаний в месяц
 * @property string $templateId - Идентификатор шаблона
 * @property string $template_name - Идентификатор шаблона
 * @property string $actionType - Тип для шаблона - редактирование или создание
 * @property string $product_type - Тип кредитного продукта для Сбер
*/
class SavePaymentResult extends CrmPayment implements CrmPaymentInterface
{
    public const PAYMENT_METHOD = 'savePaymentResult';

    /**
     * TODO: extract common properties
     * @param SavePaymentResultRequest|Request $request
     */
    public function fillModel(Request $request): void
    {
        $this->fill([
            'contract_id' => $request->get('o_cid'),
            'initiator' => $request->get('o_initiator'),
            'account_id' => $request->get('o_aid'),
            'templateId' => $request->get('o_template_id'),
            'template_name' => $request->get('o_template_name'),
            'ptype' => $request->get('o_ptype', 'PaymentCard'),
            'payment_type' => $request->get('o_payment_type', 'OnlinePayCard'),
            'actionType' => $request->get('o_action_type'),
            'pps_trxid' => $request->get('trx_id'),
            'transaction_id' => $request->get('o_idtrx'),
            'trx_datetime' => $request->get('ts'),
            'rrn' => $request->get('p_rrn'),
            'card_id' => $request->get('p_storage_card_ref', 'N/A'),
            'cardno_masked' => $request->get('p_maskedPan', 'N/A'),
            'card_expires_at' => $request->get('p_storage_card_expDt', 'N/A'),
            'card_saved' => $request->get('p_storage_card_registered') ? 'Y' : 'N',
            'card_type' => $request->get('o_card_type'),
            'amount' => $request->get('o_amount'),
            'p_storage_card_recurrent' => $request->get('p_storage_card_recurrent'),
            'result_code' => $request->get('result_code'),
            'ext_result_code' => $request->get('ext_result_code') ?: null,
            'result_msg' => $request->get('ext_result_code') ? "Ext result code: {$request->get('ext_result_code') }" : null,
            'currency' => $this->getCurrency($request->get('o_currency')),
            'login' => $request->get('o_login'),
            'email' => $request->get('o_email'),
            'sumMin' => $request->get('o_sum_min'),
            'sumMax' => $request->get('o_sum_max'),
        ]);
    }

    /**
     * @return bool
     */
    public function saveTemplate(): bool
    {
        return (bool)$this->template_name || $this->isRecurrent();
    }
}