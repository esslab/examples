<?php

namespace App\Models\Smartpay;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $user_id
 * @property int $refresh_token
 * @property int $iv
 * @property int $expires_in
 * @property Carbon $updated_at
 */
class SBBOLTokens extends Model
{

}
