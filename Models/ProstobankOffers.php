<?php

namespace App\Models\Smartpay;


use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountPaymentModel
 *
 * @package App\Models\Smartpay
 */
class ProstobankOffers extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'prostobank_offers';
    protected $primaryKey = 'client_id';

    protected $casts = [
        'client_id' => 'string',
    ];
}