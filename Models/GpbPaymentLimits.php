<?php

namespace App\Models\CRM\Smartpay;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GpbPaymentLimits
 * @package App\Models\CRM\Smartpay
 * @property string $contract_id - идентификатор контракта
 * @property string $last_payment - дата последнего платежа в формате Y-m-d
 * @property float $total_paid_sum - общая сумма платежей за дату
 */
class GpbPaymentLimits extends Model
{
    protected $connection = 'pgsql';
    protected $primaryKey = 'contract_id';
    protected $fillable = ['contract_id', 'last_payment', 'total_paid_sum'];

    /**
     * @param string $contract_id
     * @param float $amount
     */
    public function updateLimits(string $contract_id, float $amount): void
    {
        $today = date('Y-m-d');

        if ($this->last_payment === $today) {
            $this->total_paid_sum += $amount;
        } else {
            $this->last_payment = $today;
            $this->total_paid_sum = $amount;
        }

        $this->save();
    }
}