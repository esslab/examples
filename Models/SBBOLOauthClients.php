<?php

namespace App\Models\Smartpay;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $user_id
 * @property int $secret
 * @property string $iv
 * @property int $client_id
 */
class SBBOLOauthClients extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'sbbol_oauth_clients';
    public $fillable = ['name', 'secret', 'user_id', 'client_id', 'iv'];
}
