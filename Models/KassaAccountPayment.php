<?php


namespace App\Models\CRM\Smartpay;

use YooKassa\Model\Metadata;
use YooKassa\Model\PaymentInterface;

/**
 * Class KassaAccountPayment
 * @package App\Models\CRM\Smartpay
 */
class KassaAccountPayment extends AccountPayment
{
    public const THIRD_PARTY_PAYMENT = 'third_party_payment';
    private Metadata $metadata;
    public const BANK = 'Sberbank';
    public const PARTNER = 'Yandex';
    public string $channel = 'ya_kassa';
    public const STATUSES = [
        'Pending'      => self::PENDING,
        'Canceled'     => self::CANCEL,
        'Succeeded'    => self::SUCCESS,
        'In Progress'  => self::PROGRESS,
    ];

    /**
     * @param PaymentInterface $payment
     */
    public function fillFromBankPayment($payment): void
    {
        $this->metadata = $payment->getMetadata();

        $this->id_transaction = $this->metadata['IdTransaction'];
        $this->trx_id = $payment->getId();
        $this->contract = $this->metadata['Contract'] ?? null;
        $this->account_number = $payment->getPaymentMethod()['payer_bank_details']['account'] ?? null;
        $this->amount = $payment->getAmount()['_value'];
        $this->bank_status = ucfirst($payment->getStatus());
        $this->crm_status = $this->getStatus();
        $this->date_time_transaction = $payment->getCreatedAt();
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->metadata['Login'] ?? null;
    }
}