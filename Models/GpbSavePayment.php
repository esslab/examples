<?php

namespace App\Models\CRM\Smartpay;

use App\Services\Smartpay\AcquiringService;
use Cache;

/**
 * Class PaymentData
 * @package App\Models\CRM\Smartpay
 * @property string|int $code - Код ответа
 * @property string $description - Описание ответа
 * @property string $merchantTransactionId - Идентификатор транзакции магазина
 * @property string $purchase_short_desc - Краткое описание товара
 * @property string $purchase_long_desc - Полное описание товара
 * @property string $account_id - Номер счета
 * @property string $account_amount - Сумма
 * @property string $account_currency - Валюта платежа
 * @property string $account_exponent - Экспонента валюты
 * @property bool $isRecurrent - Рекуррентный платеж
 * @property string $card_id - Интеграционный идентификатор карты в Банке
 * @property string $responseType - Тип ответа
 * @property string $present - Наличие платежной карты
 * @property string $email - Адрес электронной почты пользователя
 * @property string $contractId - Номер договора
 * @property string $login - Логин пользователя
 */
class GpbSavePayment extends GpbPayment implements CardPaymentInterface
{
    public const PAYMENT_TYPE = 'register-payment-response';

    /**
     * @param SavePaymentResult $crmPayment
     * @param null $crmResponse
     */
    public function makeAfterPaymentActions($crmResponse, $crmPayment)
    {
        if ($crmPayment->result_code != GpbPayment::PAYMENT_SUCCESS_CODE) {
            Cache::put('gpb_payment_' . sha1($crmPayment->transaction_id), GpbPayment::FAILED_MESSAGE, now()->addMinutes(10));
        }

        if ($crmPayment->saveTemplate()) {
            if ($crmPayment->p_storage_card_recurrent === 'Y' || !isset($crmPayment->p_storage_card_recurrent)) {
                AcquiringService::log('Сохраняем шаблон ' . $crmPayment->template_name);
                AcquiringService::saveTemplate($crmPayment);
                AcquiringService::log('Шаблон сохранен');
            } elseif ($crmPayment->p_storage_card_recurrent === 'N') {
                AcquiringService::log('Невозможно сохранить шаблон ' . $crmPayment->template_name);
                Cache::put('gpb_payment_' . sha1($crmPayment->transaction_id), GpbPayment::FAILED_RECURRENT_MESSAGE, 60 * 10);
            }
        }
    }
}
