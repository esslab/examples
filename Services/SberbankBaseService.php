<?php

namespace App\Services\Smartpay;


abstract class SberbankBaseService extends PayableService
{
    protected string $base_url;
    protected string $access_token;
    /**
     * @var string канал для логирования
     */
    protected string $channel = 'kvk';

    /**
     * YandexKassaService constructor.
     * @param null|string $access_token
     * @param bool $set_token
     */
    public function __construct(?string $access_token, $set_token = true)
    {
        if ($set_token) {
            $this->setAccessToken($access_token);
        }
    }

    /**
     * @param string $access_token
     */
    public function setAccessToken(string $access_token): void
    {
        if (!$access_token) {
            abort(400,'An access token is not set');
        }

        $this->access_token = $access_token;
        $this->client->setAuthHeader($access_token);
    }

    /**
     * @param $base64url
     * @return bool|string
     */
    public function base64url_decode($base64url)
    {
        $base64 = strtr($base64url, '-_', '+/');
        return base64_decode($base64);
    }
}
