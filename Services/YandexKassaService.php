<?php

namespace App\Services\Smartpay;

use App\Clients\Smartpay\YaKassaClient;
use App\Helpers\SmartpayHelper;
use App\Models\Contract;
use App\Models\CRM\Smartpay\AccountPayment;
use App\Models\CRM\Smartpay\KassaAccountPayment;
use Carbon\Carbon;
use Log;
use YooKassa\Client;
use App\Services\CRM\Smartpay\SmartpayService;
use YooKassa\Common\Exceptions\{ApiException, BadApiRequestException,
    ForbiddenException, InternalServerError, NotFoundException, ResponseProcessingException,
    TooManyRequestsException, UnauthorizedException};
use YooKassa\Request\Payments\CreatePaymentResponse;

class YandexKassaService extends PayableService
{
    private const RETURN_PATH = '/finances';
    protected string $channel = 'ya_kassa';
    public string $partner;

    /**
     * YandexKassaService constructor.
     */
    public function __construct()
    {
        $this->partner = KassaAccountPayment::PARTNER;
        $this->client = new Client(new YaKassaClient());
        $this->client->setAuth(config('smartpay.providers.kassa.shop_id'), config('smartpay.providers.kassa.secret_key'));
    }

    /**
     * @param Contract $contract
     * @param string $trx_id
     * @param array $params
     * @return CreatePaymentResponse
     * @throws ApiException
     * @throws BadApiRequestException
     * @throws ForbiddenException
     * @throws InternalServerError
     * @throws NotFoundException
     * @throws ResponseProcessingException
     * @throws TooManyRequestsException
     * @throws UnauthorizedException
     */
    public function payment(
        Contract $contract,
        string $trx_id,
        array $params
    ): CreatePaymentResponse {
        Log::channel('ya_kassa')->info('request: [' . Carbon::now() . '] Id Trx:' . $trx_id . ', Amount: ' . $params['amount']);

        $payment = $this->client->createPayment(
            [
                'amount' => [
                    'value' => $params['amount'],
                    'currency' => 'RUB',
                ],
                'payment_method_data' => [
                    'type' => 'b2b_sberbank',
                    'payment_purpose' => SmartpayHelper::prepareDescription(
                        $contract,
                        "TransId [$trx_id]",
                        $params['type'] ?? 'default'
                    ),
                    'vat_data' => [
                        'type' => 'calculated',
                        'rate' => '20',
                        'amount' => [
                            'value' => SmartpayHelper::countVAT($params['amount']),
                            'currency' => 'RUB',
                        ],
                    ],
                ],
                'metadata' => [
                    'IdTransaction' => $trx_id,
                    'Contract' => $contract->number,
                    'Login' => auth()->user()->login,
                ],
                'confirmation' => [
                    'type' => 'redirect',
                    'return_url' => config('api.lk_url')  . self::RETURN_PATH,
                ],
                'capture' => true,
                'description' => SmartpayHelper::prepareDescription(
                    $contract,
                    "TransId [$trx_id]",
                    $params['type'] ?? 'default'
                ),
            ],
            uniqid('', true)
        );

        Log::channel('ya_kassa')->info('response: [' . Carbon::now() . ']' . json_encode($payment));

        return $payment;
    }

    /**
     * @param AccountPayment $payment
     * @return AccountPayment
     * @throws \Exception
     */
    public function getPayment(AccountPayment $payment): AccountPayment
    {
        try {
            $payment_options = $this->client->getPaymentInfo($payment->getMerchId());
            $payment->fillFromBankPayment($payment_options);
        } catch (\Exception $e) {
            Log::channel('ya_kassa')->info('Getting payment failure: [' . Carbon::now() . ']' . json_encode($payment_options ?? ['Unknown payment ' . $payment->getMerchId()]));

            throw $e;
        }

        return $payment;
    }

    /**
     * @param AccountPayment $payment
     * @throws \App\Exceptions\CRMException
     * @throws \Exception
     */
    public function savePaymentToCRM(AccountPayment $payment): void
    {
        $savePaymentToCRM = new SmartpayService();
        $savePaymentToCRM->saveAccountPayment($payment);
    }

    /**
     * @param $crm_payment
     * @param $bank_payment
     * @return AccountPayment
     */
    public function getPaymentInstance($crm_payment): AccountPayment
    {
        return new KassaAccountPayment($crm_payment);
    }
}
