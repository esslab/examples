<?php

namespace App\Services\Smartpay;

use App\Models\CRM\Smartpay\AccountPayment;
use App\Models\Smartpay\AccountPaymentModel;
use Log;

abstract class PayableService
{
    protected const SUCCEEDED = 'succeeded';
    protected const CANCELED = 'canceled';
    private string $channel;
    protected $client;

    /**
     * @param AccountPayment $payment
     * @return AccountPayment
     */
    abstract protected function getPayment(AccountPayment $payment): AccountPayment;
    abstract protected function getPaymentInstance($crm_payment): AccountPayment;
    abstract protected function savePaymentToCRM(AccountPayment $payment): void;

    /**
     * @param array $crm_payment
     * @param bool $force_cancel
     * @return AccountPayment
     */
    public function updateStatuses(array $crm_payment, $force_cancel = false): AccountPayment
    {
        $payment_instance = $this->getPaymentInstance($crm_payment);
        $this->channel = $payment_instance->channel;

        if ($force_cancel) {
            $payment_instance->bank_status = $payment_instance::FORCE_CANCELLED;
            $this->cancelPayment($payment_instance, $force_cancel);
        }

        if ($payment_instance->getMerchId()) {
            $payment = $this->getPayment($payment_instance);

            if ($payment->isExpired()) {
                $payment->bank_status = $payment::EXPIRED;
                $this->cancelPayment($payment_instance);
            }
        } else {
            $this->logAndAbort('There is no external id');
        }

        if ($payment->getStatus() === $payment::ERROR) {
            $this->logAndAbort('Workflow fault');
        }

        if (!$payment->getStatus()) {
            $this->logAndAbort('Unknown status ' . $payment->bank_status);
        }

        $accountPayment = AccountPaymentModel::where('id_transaction', $payment->id_transaction)->first();

        if ($accountPayment) {
            $payment->contract = $accountPayment->contract;
        }

        if ($payment->getStatus() !== $payment::INITIAL_STATUS) {
            $payment->crm_status = $payment->getStatus();
            $this->savePaymentToCRM($payment);
        } else {
            $this->logAndAbort('Payment ' . $payment->id_transaction . ' was skipped due to pending status');
        }

        $this->updateLocalStatus($payment, $accountPayment);

        return $payment;
    }

    /**
     * @param AccountPayment $payment
     * @param null $accountPayment
     */
    private function updateLocalStatus(AccountPayment $payment, $accountPayment = null): void
    {
        if ($accountPayment === null) {
            $accountPayment = AccountPaymentModel::where('id_transaction', $payment->id_transaction)->first();
        }

        if ($accountPayment) {
            $accountPayment->status = $payment->bank_status;
            $accountPayment->save();
        }
    }

    /**
     * @param $force_cancel
     * @param AccountPayment $payment
     */
    private function cancelPayment(AccountPayment $payment, $force_cancel = false): void
    {
        $payment->crm_status = $payment::CANCEL;
        $this->savePaymentToCRM($payment);

        $message = 'Payment ' . $payment->id_transaction . ' was cancelled due to age ' . $payment->date_time_transaction;

        if ($force_cancel) {
            $message = 'Payment ' . $payment->id_transaction . ' was force-cancelled ........';
        }

        $this->updateLocalStatus($payment);
        $this->logAndAbort($message);
    }

    /**
     * @param string $message
     */
    private function logAndAbort(string $message): void
    {
        Log::channel($this->channel)->info($message);
        abort(400, $message);
    }
}