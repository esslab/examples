<?php

namespace App\Services\Smartpay;


use App\Clients\Smartpay\RFClient;
use App\Exceptions\CRMException;
use App\Helpers\SmartpayHelper;
use App\Models\CRM\Smartpay\AccountPayment;
use App\Models\CRM\Smartpay\SbpAccountPayment;
use App\Services\CRM\Smartpay\SmartpayService;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class SbpService extends PayableService
{
    public string $payment = SbpAccountPayment::class;

    /**
     */
    public function __construct()
    {
        $this->client = new RFClient(['base_uri' => config('smartpay.providers.sbp.api_url')]);
    }

    /**
     * @param SbpAccountPayment $payment
     * @param $contract
     * @return mixed|ResponseInterface|string
     * @throws GuzzleException
     */
    public function getQR(SbpAccountPayment $payment, $contract)
    {
        $tz = $payment->tz->toOffsetName();
        $created_at = clone($payment->date_time_transaction);

        return $this->client->request('POST', 'sbp/v1/qr/register', [
            'json' => [
                'account' => config('smartpay.providers.sbp.account_no'),
                'additionalInfo' => $contract->number,
                'amount' => $payment->amount,
                'createDate' => $created_at->format('Y-m-d\\TH:i:s') . $tz,
                'currency' => 'RUB',
                'order' => $payment->id_transaction,
                'paymentDetails' => SmartpayHelper::prepareDescription($contract, "TransId_SBP [$payment->id_transaction]"),
                'qrType' => $payment::QR_TYPE,
                'qrExpirationDate' => $created_at->addHour(24)->format('Y-m-d\\TH:i:s') . $tz,
                'sbpMerchantId' => config('smartpay.providers.sbp.merchant_id'),
            ]
        ]);
    }

    /**
     * @param AccountPayment $payment
     * @return AccountPayment
     */
    public function getPayment(AccountPayment $payment): AccountPayment
    {
        $this->client->setAuthHeader(config('smartpay.providers.sbp.client_secret'));

        if ($payment_response = json_decode($this->client->request('GET', 'sbp/v1/qr/' . $payment->getMerchId() . '/payment-info'), true)) {
            if (!isset($payment_response['code'])) {
                abort(400, 'Payment response has no code filed');
            }

            if (strpos($payment_response['code'], 'ERROR') === FALSE) {
                $payment->fillFromBankPayment($payment_response);

                return $payment;
            } elseif ($payment_response['code'] === 'ERROR.NOT_FOUND') {
                $payment->bank_status = $payment::NOT_FOUND;

                return $payment;
            }
        }

        abort(400, 'Can not retrieve the payment with id ' . $payment->getMerchId() . '. Payment code is ' . $payment_response['code']);
    }

    /**
     * @param $crm_payment
     * @return AccountPayment
     */
    public function getPaymentInstance($crm_payment): AccountPayment
    {
        return new SbpAccountPayment($crm_payment);
    }

    /**
     * @param SbpAccountPayment|AccountPayment $payment
     * @throws CRMException
     */
    public function savePaymentToCRM(AccountPayment $payment): void
    {
        $savePaymentToCRM = new SmartpayService();
        $savePaymentToCRM->saveResultPaymentSBP($payment);
    }
}