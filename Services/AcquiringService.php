<?php

namespace App\Services\Smartpay;

use App\Exceptions\ApiException;
use App\Exceptions\CRMException;
use App\Http\Controllers\V2\Smartpay\AcquiringController;
use App\Http\Requests\V2\Smartpay\GpbPaymentRequest;
use App\Http\Responses\GPBPaymentResponse;
use App\Models\CRM\Smartpay\CheckPaymentAvail;
use App\Models\CRM\Smartpay\CrmPayment;
use App\Models\CRM\Smartpay\CrmPaymentInterface;
use App\Models\CRM\Smartpay\GpbCheckPayment;
use App\Models\CRM\Smartpay\GpbPayment;
use App\Models\CRM\Smartpay\GpbPaymentLimits;
use App\Models\CRM\Smartpay\GpbSavePayment;
use App\Models\CRM\Smartpay\SavePaymentResult;
use App\Services\CRM\Smartpay\SmartpayService;
use App\User;
use Log;
use Cache;

class AcquiringService extends SmartpayService
{
    /**
     * @param GpbPaymentRequest $request
     * @param CheckPaymentAvail|SavePaymentResult|CrmPayment $crmPayment
     * @param GpbCheckPayment|GpbSavePayment|GpbPayment $gpbPayment
     * @return mixed
     */
    public static function checkOrSave(GpbPaymentRequest $request, CrmPayment $crmPayment, GpbPayment $gpbPayment)
    {
        try {
            self::makePayment($request, $crmPayment, $gpbPayment);
            $gpbPayment->setSucceededResponse();
        } catch (\Exception $exception) {
            $gpbPayment->setFailedResponse($exception);
            Cache::put('gpb_payment_' . sha1($crmPayment->transaction_id), $gpbPayment::FAILED_MESSAGE, 60 * 10);
        }

        return (new GPBPaymentResponse($gpbPayment))->response();
    }

    /**
     * @param GpbPaymentRequest $request
     * @param CrmPayment $crmPayment
     * @param GpbCheckPayment|GpbSavePayment|GpbPayment $gpbPayment
     * @throws \Exception
     */
    private static function makePayment(GpbPaymentRequest $request, CrmPayment $crmPayment, GpbPayment $gpbPayment): void
    {
        $crmResponse = self::makeCRMPayment($request, $crmPayment, $gpbPayment);

        $gpbPayment->makeAfterPaymentActions($crmResponse, $crmPayment);
    }

    /**
     * @param GpbPaymentRequest $request
     * @param CrmPayment|CrmPaymentInterface $crmPayment
     * @param GpbPayment $gpbPayment
     * @return mixed
     * @throws \Exception
     */
    private static function makeCRMPayment(
        GpbPaymentRequest $request,
        CrmPayment $crmPayment,
        GpbPayment $gpbPayment
    ) {
        $crmPayment->fillFromRequest($request, $gpbPayment->fillFromCRMPayment());

        return $crmPayment->paymentService->pay($crmPayment);
    }

    /**
     * @param CrmPayment $crmPayment
     * @return array
     * @throws ApiException
     */
    public function pay(CrmPayment $crmPayment): array
    {
        if ($crmPayment->isRecurrent()) {
            return [];
        }

        $paymentMethod = $crmPayment::PAYMENT_METHOD;

        if ($this->shouldCheckLimits($paymentMethod)) {
            $this->checkGpbLimits(
                $crmPayment->contract_id,
                $crmPayment->convertToDecimal($crmPayment->amount)
            );
        }

        self::log('Запрос к CRM ' . $paymentMethod . '...' . 'Transaction ID: ' . $crmPayment->transaction_id);
        $response = $this->$paymentMethod($crmPayment);
        self::log('Успешно!');

        if ($this->shouldUpdateLimits($paymentMethod, $crmPayment->result_code ?? '')) {
            $this->updateGpbLimits(
                $crmPayment->contract_id,
                $crmPayment->convertToDecimal($crmPayment->amount)
            );
        }

        return $response;
    }

    /**
     * TODO: match properties of SavePaymentResult and CRM ones
     * @param SavePaymentResult|CrmPaymentInterface $paymentResult
     * @return array
     * @throws CRMException
     */
    public static function saveTemplate(SavePaymentResult $paymentResult): array
    {
        return (new SmartpayService($paymentResult->login))->setupFuelContractAutoPayment([
            'action_type' => $paymentResult->actionType,
            'template_id' => $paymentResult->templateId,
            'contract_id' => $paymentResult->contract_id,
            'payment_type' => $paymentResult->payment_type,
            'accept' => $paymentResult->accept,
            'email' => $paymentResult->email,
            'card_type' => $paymentResult->card_type,
            'amount' => $paymentResult->convertToDecimal($paymentResult->amount),
            'currency' => $paymentResult->currency,
            'account_no' => $paymentResult->account_no,
            'bcard_id' => $paymentResult->card_id,
            'cardno_masked' => $paymentResult->cardno_masked,
            'sum_min' => $paymentResult->sumMin,
            'sum_max' => $paymentResult->sumMax,
            'template_name' => $paymentResult->template_name ?? 'Recurrent Payment Template',
        ]);
    }

    /**
     * @param string $method
     * @return bool
     */
    private function shouldCheckLimits(string $method): bool
    {
        return $method === CheckPaymentAvail::PAYMENT_METHOD
            && AcquiringController::isLimitEnabled()
            ;
    }

    /**
     * @param string $method
     * @param string $result_code
     * @return bool
     */
    private function shouldUpdateLimits(string $method, string $result_code): bool
    {
        return $method === SavePaymentResult::PAYMENT_METHOD
            && (int)$result_code === GpbPayment::PAYMENT_SUCCESS_CODE
            && AcquiringController::isLimitEnabled()
            ;
    }

    /**
     * @param string $contract_id
     * @param float $amount
     */
    private function updateGpbLimits(string $contract_id, float $amount): void
    {
        $limit = GpbPaymentLimits::firstOrNew([
            'contract_id' => $contract_id
        ]);

        $limit->updateLimits($contract_id, $amount);
    }

    /**
     * @param string $contract_id
     * @param float $amount
     * @throws ApiException
     */
    private function checkGpbLimits(string $contract_id, float $amount): void
    {
        if (!(new AcquiringController())->isAvailableToPay($contract_id, $amount)) {
            throw new ApiException(AcquiringController::OVERLIMIT_MESSAGE);
        }
    }

    /**
     * @param GpbPaymentRequest $request
     * @return bool
     */
    private static function failedPaymentRequest(GpbPaymentRequest $request): bool
    {
        return $request->result_code && (int)$request->result_code !== GpbPayment::PAYMENT_SUCCESS_CODE;
    }

    /**
     * @param string $log
     */
    public static function log(string $log): void
    {
        if (config('app.debug')) {
            Log::channel('smartpay_log')->info($log . "\n");
        }
    }
}
