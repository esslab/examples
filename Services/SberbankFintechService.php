<?php

namespace App\Services\Smartpay;

use App\Exceptions\ApiException;
use App\Helpers\SmartpayHelper;
use App\Models\Contract;
use App\Models\CRM\Smartpay\AccountPayment;
use App\Models\CRM\Smartpay\SberAccountPayment;
use App\Models\Smartpay\SBBOLCorcheckAccessTokens;
use App\Models\Smartpay\SBBOLOauthAccessTokens;
use App\Models\Smartpay\SBBOLTokens;
use GuzzleHttp\Exception\GuzzleException;
use Log;

class SberbankFintechService extends SberbankService
{
    private ?string $user_id;

    /**
     * YandexKassaService constructor.
     * @param null|string $user_id
     * @param bool $set_token
     * @param $product
     */
    public function __construct($product = SberbankService::KVK, ?string $user_id = null, bool $set_token = true)
    {
        $this->base_url = config('smartpay.providers.sbbol.api_url');
        $this->user_id = $user_id;

        parent::__construct($product, $user_id, $set_token);

        $this->client->setHeader($this->client::CONTENT_JSON);
    }

    /**
     * Формирование заявки на кредит
     *
     * @param SberAccountPayment $payment
     * @param Contract $contract
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function createCreditRequest(SberAccountPayment $payment, Contract $contract)
    {
        return json_decode($this->client->request('POST', 'v1/credit-requests', [
            'body' => json_encode([
                'account' => config('smartpay.providers.sbbol.'. $payment->product . '.account_no'),
                'amount' => $payment->amount,
                'creditAmount' => $payment->credit_amount,
                'creditProductCode' => $payment->credit_product_code,
                'creditTerm' => $payment->credit_term,
                'externalId' => $payment->id_transaction,
                'orderId' => $payment->order_id,
                'orderUrl' => config('api.lk_url') . SberAccountPayment::CREDIT_OFFER_PAGE,
                'purpose' => SmartpayHelper::prepareDescription($contract, "TransId_SBER [$payment->id_transaction]") . ' НДС 20 % - ' . SmartpayHelper::countVAT($payment->amount) . ' рублей',
                'vatAmount' => (string) SmartpayHelper::countVAT($payment->amount)
            ])
        ]), true);
    }

    /**
     * Получение информации по кредитным предложениям
     *
     * @param string $lawForm
     * @param string|null $clientId
     * @return \Psr\Http\Message\ResponseInterface|string
     */
    public function getCreditOffers(string $lawForm = '', string $clientId = null)
    {
        $clientId = $clientId ?? config('smartpay.providers.sbbol.' . $this->product . '.client_id');

        return json_decode($this->client->request('GET', 'v1/credit-offers?' .
            'lawForm=' . $lawForm .
            '&clientID=' . $clientId,
            []
        ),true);
    }

    /**
     * Запрос статуса платежного документа
     *
     * @return mixed
     */
    public function getCryptoInfo()
    {
        $this->setServiceToken();

        $crypto = json_decode(
            $this->client->request('GET', 'v1/crypto'),
            true
        );

        return $crypto;
    }

    /**
     * @return SBBOLTokens
     */
    private function getClientCryptoInfo(): SBBOLTokens
    {
        if ($this->product === self::CORCHECKOUT) {
            $user_crypto = SBBOLCorcheckAccessTokens::where('user_id', $this->user_id)->first();
        }

        if ($this->product === self::KVK) {
            $user_crypto = SBBOLOauthAccessTokens::where('user_id', $this->user_id)->first();
        }

        if (!empty($user_crypto)) {
            $crypto = $this->getCryptoInfo();

            if ($user_crypto->cert_bank_uuid !== $crypto['certBankUuid']) {
                $user_crypto->cert_bank_uuid = $crypto['certBankUuid'];
                $user_crypto->cert_bank = $crypto['certBank'];
                $user_crypto->save();
            }
        }

        return $user_crypto;
    }

    /**
     * @param string $id
     * @param array $crm_payment
     * @return AccountPayment
     */
    public function getPayment(AccountPayment $payment): AccountPayment
    {
        $payment_response = $this->getExternalPayment($payment->getMerchId());
        $payment->fillFromBankPayment($payment_response);

        if ($payment->bank_status !== 'NOT_FOUND') {
            $this->getAmountAndVerify($payment);
        }

        return $payment;
    }

    /**
     * @param AccountPayment $payment
     * @param bool $verify_status
     * @return AccountPayment
     */
    public function getAmountAndVerify(AccountPayment $payment, $verify_status = false): AccountPayment
    {
        try {
            $payload = $this->getPaymentDoc(
                $payment,
                config('smartpay.providers.sbbol.verify_signature')
            );
            $payment->amount = $payload['amount'];
        } catch (GuzzleException $e) {
            if (!$payment) {
                abort(400, 'Не был получен объект платежа: ' . $e->getMessage());
            }
        }

        if ($verify_status) {
            if (SberAccountPayment::SUCCESS === SberAccountPayment::STATUSES[$payment['status']]) {
                return $payment;
            }

            abort(400, 'Запрошенный статус не совпадает с успешным ' . $payment['status']);
        }

        return $payment;
    }

    /**
     * Запрос статуса платежного документа
     *
     * @param string $id
     * @return array|mixed
     */
    private function getExternalPayment(string $id)
    {
        $payment = [];

        try {
            $payment = json_decode($this->client->request('GET', 'v1/payments/' . $id . '/state'), true);
        } catch (GuzzleException $e) {
            if ($e->getCode() === 404) {
                $payment['bankStatus'] = 'NOT_FOUND';
            } else {
                abort($e->getCode(), 'Не был получен объект платежа: ' . $e->getMessage());
            }
        }

        return $payment;
    }

    /**
     * @param AccountPayment $payment
     * @return mixed
     */
    private function getPaymentDoc(AccountPayment $payment, $verify_signature = true)
    {
        $this->client->setHeader($this->client::ACCEPT_JOSE);
        $payment_jwt = $this->client->request('GET', 'v1/payments/' . $payment->getMerchId());
        Log::channel('kvk')->info($payment_jwt);
        $payment_jwt_parts = explode('.', $payment_jwt);
        $header = json_decode(base64_decode($payment_jwt_parts[0]), true);
        $payload = json_decode($this->base64url_decode($payment_jwt_parts[1]), true);
        $signature = $this->base64url_decode($payment_jwt_parts[2]);
        $this->client->setHeader($this->client::ACCEPT_JSON);

        if ($verify_signature) {
            $this->getClientCryptoInfo();
            $this->verifySignature($signature, $header, $payment_jwt_parts[0] . '.' . $payment_jwt_parts[1]);
        }

        return $payload;
    }

    /**
     * @param SberAccountPayment $payment
     * @param Contract $contract
     * @param string $credit_contract_number
     */
    public function formInvoiceAny(SberAccountPayment $payment, Contract $contract, string $credit_contract_number)
    {
        return json_decode($this->client->request('POST', 'v1/payments/from-invoice-any', [
            'body' => json_encode([
                'amount' => $payment->amount,
                'date' => $payment->date_time_transaction->format('Y-m-d'),
                'deliveryKind' => 'электронно',
                'expirationDate' => $payment->date_time_transaction->addDays($payment::CANCEL_AFTER/24)->format('Y-m-d'),
                'orderNumber' => $payment->order_id,
                'externalId' => $payment->id_transaction,
                'payeeAccount' => config('smartpay.providers.sbbol.'. $payment->product . '.account_no'),
                'payeeBankBic' => config('smartpay.providers.sbbol.bank_details.bik'),
                'payeeBankCorrAccount' => config('smartpay.providers.sbbol.bank_details.bank_corr_acc'),
                'payeeInn' => config('smartpay.providers.sbbol.bank_details.inn'),
                'payeeKpp' => config('smartpay.providers.sbbol.bank_details.kpp'),
                'payeeName' => config('smartpay.providers.sbbol.bank_details.org_name'),
                'priority' => "5",
                'purpose' => SmartpayHelper::prepareDescription($contract, "TransId_SBER [$payment->id_transaction]") . ' НДС 20 % - ' . SmartpayHelper::countVAT($payment->amount) . ' рублей',
                'urgencyCode' => 'INTERNAL',
                'isPaidByCredit' => true,
                'creditContractNumber' => $credit_contract_number,
                'vat' => [
                    'amount' => (string) SmartpayHelper::countVAT($payment->amount),
                    'rate' => "20",
                    'type' => 'INCLUDED'
                ],
            ]),
        ]), true);
    }

    /**
     * @param SberAccountPayment $payment
     * @param Contract $contract
     * @return mixed
     */
    public function formInvoice(SberAccountPayment $payment, Contract $contract)
    {
        return json_decode($this->client->request('POST', 'v1/payments/from-invoice', [
            'body' => json_encode([
                'amount' => $payment->amount,
                'date' => $payment->date_time_transaction->format('Y-m-d'),
                'deliveryKind' => 'электронно',
                'expirationDate' => $payment->date_time_transaction->addDays($payment::CANCEL_AFTER/24)->format('Y-m-d'),
                'orderNumber' => $payment->order_id,
                'externalId' => $payment->id_transaction,
                'payeeAccount' => config('smartpay.providers.sbbol.'. $payment->product . '.account_no'),
                'priority' => '5',
                'purpose' => SmartpayHelper::prepareDescription($contract, "TransId [$payment->id_transaction]", $payment->type, $payment->inn, $payment->login) . ' НДС 20 % - ' . SmartpayHelper::countVAT($payment->amount) . ' рублей',
                'urgencyCode' => 'NORMAL',
                'vat' => [
                    'amount' => (string) SmartpayHelper::countVAT($payment->amount),
                    'rate' => '20',
                    'type' => 'INCLUDED'
                ],
            ]),
        ]), true);
    }

    /**
     * @param string $signature
     * @param $header
     */
    private function verifySignature(string $signature, $header, $hash) {
        $accountTokens = SBBOLOauthAccessTokens::where('cert_bank_uuid', $header['kid'])->first();

        $signature = mb_strtoupper(bin2hex($signature));
        $signature = str_split($signature, 2);
        $signature = array_reverse($signature);
        $signature = implode(' ', $signature);

        try {
            $CPCert = new \CPCertificate();
            $CPCert->Import($accountTokens->cert_bank);
            $CPHd = new \CPHashedData();
            $CPHd->set_Algorithm(CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_256);
            $CPHd->set_DataEncoding(BASE64_TO_BINARY);
            $hash = base64_encode($hash);
            $CPHd->Hash($hash);
            $CPRs = new \CPRawSignature();
            $CPRs->VerifyHash($CPHd, $signature, $CPCert);
            Log::channel('kvk')->info('Signature was verified.');
        } catch (\Exception $e) {
            throw new ApiException('Signature was not verified. ' . $e->getMessage(), 400);
        }
    }
}
