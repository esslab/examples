<?php

namespace App\Services\Smartpay;

use App\Models\CRM\Smartpay\AccountPayment;
use App\Support\GuidGenerator;
use Cache;
use function GuzzleHttp\Psr7\build_query;

class SberbankAuthService extends SberbankService
{
    private const REDIRECT_URL = '/api/v2/payments/sbbol/oauth/authorize';

    /**
     * YandexKassaService constructor.
     * @param string $product
     * @param string|null $user_id
     * @param bool $set_token
     * @throws \Exception
     */
    public function __construct(string $product = self::KVK, string $user_id = null, bool $set_token = true)
    {
        $user_id = $user_id ?? auth()->user()->id ?? self::getUserByNonceOrAbort(request()->nonce);
        $this->base_url = config('smartpay.providers.sbbol.sso_url');

        parent::__construct($product, $user_id, $set_token);
    }

    /**
     * @param string|null $product
     * @param string $params
     * @return string
     * @throws \Exception
     */
    public static function getAuthorizationLink(?string $product, array $params = []): string
    {
        $product = $product ?: self::KVK;
        [$state, $nonce] = self::storeAuthData($product);

        if ($params) {
            Cache::put($nonce . '.params', json_encode([
                'contract' => $params['contract'],
                'email' => $params['email'],
                'amount' =>  $params['amount'],
                'login' =>  auth()->user()->login,
                'user_id' =>  auth()->user()->id,
                'type' =>  $params['type'],
            ]), 600);
        } else {
            $just_auth = true;
        }

        $options = [
            'response_type' => 'code',
            'scope' => config('smartpay.providers.sbbol.' . $product .'.scope'),
            'client_id' => config('smartpay.providers.sbbol.' . $product . '.client_id'),
            'state' => $state,
            'nonce' => $nonce,
            'redirect_uri' => config('app.api_url') . self::REDIRECT_URL . '?product=' . $product,
            'userType' => 'WEB',
            'loginDCB' => true,
            'location' => config('app.api_url'),
            'code' => GuidGenerator::generate() . '-1',
            'just_auth' => $just_auth ?? false
        ];

        return config('smartpay.providers.sbbol.sso_domain') . '/ic/sso/api/v2/oauth/authorize?' . build_query($options);
    }

    /**
     * @param string $product
     * @return array
     * @throws \Exception
     */
    private static function storeAuthData(string $product): array
    {
        $state = GuidGenerator::generate();
        $nonce = GuidGenerator::randomString();
        $session = request()->sbbol_sid;
        Cache::put($nonce, auth()->user()->id, 600);
        Cache::put('sbbol.inn.' . auth()->user()->id, auth()->user()->client_info()->inn, 600);
        Cache::put('sbbol.session.' . $product . '.' . auth()->user()->id, $session, 600);

        return array($state, $nonce);
    }

    /**
     * @param string $nonce
     * @return array
     */
    public function restoreAuthData(string $nonce): array
    {
        $user_id = self::getUserByNonceOrAbort($nonce);
        $inn = self::getInnByUser($user_id);
        $session = $this->getSessionIdByUser($user_id);

        return [$user_id, $inn, $session];
    }

    /**
     * @param string $nonce
     * @return array
     */
    public static function restoreParams(string $nonce): array
    {
        return json_decode(Cache::get($nonce . '.params', '') , true) ?: [];
    }

    /**
     * Запрос и обновление токена
     *
     * @param string $code
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getTokens(string $code)
    {
        $client = self::getOauthClient($this->product);

        $tokens = json_decode($this->client->request('POST', 'v2/oauth/token', [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'code' => $code,
                    'client_id' => config('smartpay.providers.sbbol.' . $this->product . '.client_id'),
                    'client_secret' => !empty($client->secret) ? $this->decrypt($client, $client->secret) : config('smartpay.providers.sbbol.' . $this->product . '.client_secret'),
                    'redirect_uri' => config('app.api_url') . self::REDIRECT_URL . '?product=' . $this->product,
                ]
            ]), true);

        $this->setAccessToken($tokens['access_token']);

        return $tokens;
    }

    /**
     * Запрос информации о клиенте
     *
     * @return mixed
     */
    public function getUserInfo()
    {
        $user_jwt = $this->client->request('GET', 'v1/oauth/user-info');
        $user_jwt_parts = explode('.', $user_jwt);

        $algorithm = json_decode(base64_decode($user_jwt_parts[0]), true);
        $payload = json_decode($this->base64url_decode($user_jwt_parts[1]), true);
        $signature = base64_decode($user_jwt_parts[2]);

        return $payload;
    }

    /**
     * @param AccountPayment $payment
     * @return \App\Models\CRM\Smartpay\AccountPayment|\App\Models\CRM\Smartpay\SberAccountPayment|\YooKassa\Model\PaymentInterface
     */
    protected function getPayment(AccountPayment $payment): AccountPayment {
        return (new SberbankFintechService($payment->product))->getPayment($payment);
    }

    /**
     * Обнолвение secret
     *
     * @param string $secret
     * @param string $new_secret
     * @param string $client_id
     * @return void
     */
    public function updateSecret(string $secret, string $new_secret, string $client_id): void
    {
        json_decode($this->client->request('POST', 'v1/change-client-secret', [
            'form_params' => [
                'access_token' => $this->access_token,
                'client_secret' => $secret,
                'new_client_secret' => $new_secret,
                'client_id' => $client_id,
            ]
        ]), true);
    }

    /**
     * @param null|string $nonce
     * @return string
     */
    public static function getUserByNonceOrAbort(?string $nonce): string
    {
        if (!$user_id = Cache::get($nonce, 0)) {
            abort(400, 'The nonce did not match or invalid scope');
        }

        return $user_id;
    }

    /**
     * @param $user_id
     * @return string
     */
    public static function getInnByUser($user_id): string
    {
        return Cache::get('sbbol.inn.' . $user_id, 0);
    }
}
