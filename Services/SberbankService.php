<?php

namespace App\Services\Smartpay;

use App\Clients\Smartpay\CorcheckoutClient;
use App\Clients\Smartpay\KvkClient;
use App\Exceptions\SberAuthException;
use App\Helpers\CipherHelper;
use App\Models\CRM\Smartpay\AccountPayment;
use App\Models\CRM\Smartpay\SberAccountPayment;
use App\Models\Smartpay\SBBOLCorcheckAccessTokens;
use App\Models\Smartpay\SBBOLOauthAccessTokens;
use App\Models\Smartpay\SBBOLOauthClients;
use App\Models\Smartpay\SBBOLTokens;
use Cache;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Carbon;
use App\Services\CRM\Smartpay\SmartpayService;

/**
 * @property $product Тип продута (КВК или Корчекаут)
 */
/**
 * @property string partner
 */
abstract class SberbankService extends SberbankBaseService
{
    private const CACHE_TTL = 24 * 3600;
    public string $partner = 'Sberbank';
    public const KVK = 'kvk';
    public const CORCHECKOUT = 'corcheckout';
    protected string $product;
    protected string $SBBOLTokens;

    /**
     * TODO: @var SberAccountPayment
     */
//    private SberAccountPayment $accountPayment;

    /**
     * @param string $product
     * @param string|null $user_id
     * @param bool $set_token
     * @throws \Exception
     */
    public function __construct(string $product, ?string $user_id, bool $set_token = true)
    {
        $this->setProductType($product);
        $this->partner = SberAccountPayment::PARTNER;
        $access_token = null;

        $config = [
            'base_uri' => $this->base_url,
            'cert' => storage_path('cert/' . config('smartpay.providers.sbbol.cert'))
        ];

        if ($product == self::CORCHECKOUT) {
            $this->client = new CorcheckoutClient($config);
        } else {
            $this->client = new KvkClient($config);
        }

        $user_id = $user_id ?? auth()->user()->id ?? null;

        if ($set_token && $user_id) {
            $access_token = $this->getValidAccessToken($this->getOauthTokens($user_id), $user_id);
        }

        parent::__construct($access_token, $set_token);
    }

    public function setProductType($product) {
        $this->product = $product;

        if($this->product === self::KVK) {
            $this->SBBOLTokens = SBBOLOauthAccessTokens::class;
        }

        if($this->product === self::CORCHECKOUT) {
            $this->SBBOLTokens = SBBOLCorcheckAccessTokens::class;
        }
    }

    /**
     *
     */
    public function setServiceToken()
    {
        $this->client->clearHeader('Authorization');

        $this->setAccessToken($this->getServiceToken());
        
        return $this;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getServiceToken()
    {
        return $this->getValidAccessToken($this->getOauthTokens(self::getOauthClient($this->product)->user_id), self::getOauthClient($this->product)->user_id);
    }

    /**
     * @param $user_id
     * @throws \Exception
     */
    public function getAccessToken($user_id)
    {
        return $this->getValidAccessToken($this->getOauthTokens($user_id));
    }

    /**
     * @param SBBOLTokens $oauthTokens
     * @param null $user - id пользователя, если нет session_id
     * @return string
     */
    private function getValidAccessToken(SBBOLTokens $oauthTokens, $user = null): string
    {
        $access_token = $this->getCachedToken($user);

        if ($access_token && $this->accessTokenIsAlive($oauthTokens)) {
            return $this->decrypt($oauthTokens, $access_token);
        }

        $refresh_response = $this->refreshAndSave($oauthTokens, $user);

        return $refresh_response['access_token'];
    }

    /**
     * @param $oauthTokens
     * @return bool
     */
    private function accessTokenIsAlive(SBBOLTokens $oauthTokens): bool
    {
        return Carbon::now() < $oauthTokens->updated_at->addSeconds($oauthTokens->expires_in);
    }

    /**
     * return $cached_access_token
     * @param string $user_id
     * @return mixed
     */
    private function getCachedToken($user_id = '')
    {
        $session = $this->getSessionIdByUser($user_id);

        if (!empty($session)) {
            return Cache::get('sbbol.access_token.' . $session);
        } else {
            return null;
        }
    }

    /**
     * @param $oauthTokens
     * @return string
     */
    private function getRefreshToken($oauthTokens): string
    {
        return $this->decrypt($oauthTokens, $oauthTokens->refresh_token);
    }

    /**
     * @param $oauthTokens
     * @param $access_token
     * @return string
     */
    public function decrypt($oauthTokens, string $access_token): string
    {
        if ($oauthTokens === null) {
            return '';
        }

        return CipherHelper::safeDecrypt($access_token, self::getKey($oauthTokens->user_id), $oauthTokens->iv);
    }

    /**
     * @param $user_id
     * @return string
     */
    public static function getKey(string $user_id): string
    {
        return $user_id . config('smartpay.providers.sbbol.salt');
    }

    /**
     * @param string $refresh_token
     * @return array
     * @throws GuzzleException
     */
    private function refreshTokens(string $refresh_token): array
    {
        $this->client->setHeader($this->client::CONTENT_URLENCODED);

        try {
            return json_decode($this->client->request('POST', config('smartpay.providers.sbbol.sso_url') . 'v2/oauth/token', [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refresh_token,
                    'client_id' => config('smartpay.providers.sbbol.'. $this->product .'.client_id'),
                    'client_secret' => $this->getSecret(false),
                    'scope' => config('smartpay.providers.sbbol.'. $this->product .'.scope')
                ]
            ]), true);
        } catch (ClientException $e) {
            throw new SberAuthException();
        }
    }

    /**
     * @param array $token_response
     * @param string $user_id
     * @param string|null $session
     * @return void
     */
    public function saveTokens(array $token_response, string $user_id, ?string $session): void
    {
        [$encrypted_access_token, $tokens] = $this->prepareTokens($token_response, $user_id);

        $this->saveAccessToken($session ?: $user_id, $encrypted_access_token);
        $this->saveOauthTokenData($user_id, $tokens);
    }

    /**
     * @param array $token_response
     * @param string $user_id
     * @return array
     */
    private function prepareTokens(array $token_response, string $user_id): array
    {
        $iv = CipherHelper::generateIV();
        $key = self::getKey($user_id);
        $access_token = CipherHelper::safeEncrypt($token_response['access_token'], $key, $iv);
        $refresh_token = CipherHelper::safeEncrypt($token_response['refresh_token'], $key, $iv);

        return [$access_token, [
            'refresh_token' => $refresh_token,
            'expires_in' => $token_response['expires_in'],
            'iv' => base64_encode($iv),
        ]];
    }

    /**
     * @param string $session
     * @param string $encrypted_access_token
     */
    private function saveAccessToken(string $session, string $encrypted_access_token): void
    {
        Cache::put('sbbol.access_token.' . $this->product . '.' . $session, $encrypted_access_token, self::CACHE_TTL);
    }

    /**
     * @param string $user_id
     * @param $tokens
     */
    private function saveOauthTokenData(string $user_id, $tokens): void
    {
        $this->SBBOLTokens::updateOrCreate(['user_id' => $user_id], $tokens);
    }

    /**
     * @param null|string $user_id
     * @return mixed
     * @throws \Exception
     */
    private function getOauthTokens(?string $user_id): SBBOLTokens
    {
        if ($oauthTokens = $this->SBBOLTokens::where('user_id', $user_id)->first()) {
            return $oauthTokens;
        }

        throw new SberAuthException();
    }

    /**
     * @param SBBOLTokens $oauthTokens
     * @param null $user
     * @return array
     * @throws GuzzleException
     */
    private function refreshAndSave(SBBOLTokens $oauthTokens, $user = null): array
    {
        $refresh_response = $this->refreshTokens($this->getRefreshToken($oauthTokens));
        $this->saveTokens($refresh_response, $oauthTokens->user_id, $this->getSessionIdByUser($user));

        return $refresh_response;
    }

    /**
     * @param string $product
     * @return \App\Models\Smartpay\SBBOLOauthClients|null
     */
    public static function getOauthClient(string $product): ?SBBOLOauthClients
    {
        return SBBOLOauthClients::where('name', config('smartpay.providers.sbbol.'. $product .'.client_name'))->latest()->first();
    }

    /**
     * @param bool $force_env
     * @return \Illuminate\Config\Repository|mixed|string
     */
    private function getSecret($force_env = false)
    {
        $client = self::getOauthClient($this->product);

        return (!empty($client->secret) && !$force_env) ? $this->decrypt($client, $client->secret) : config('smartpay.providers.sbbol.'. $this->product .'.client_secret');
    }

    /**
     * @param AccountPayment $payment
     * @throws \App\Exceptions\CRMException
     * @throws \Exception
     */
    public function savePaymentToCRM(AccountPayment $payment): void
    {
        $savePaymentToCRM = new SmartpayService();
        $savePaymentToCRM->saveAccountPayment($payment);
    }

    /**
     * @param $crm_payment
     * @return AccountPayment
     */
    public function getPaymentInstance($crm_payment): AccountPayment
    {
        return new SberAccountPayment($crm_payment);
    }

    /**
     * @param $user_id
     * @return string
     */
    public function getSessionIdByUser($user_id = ''): ?string
    {
        return request()->sbbol_sid ?: Cache::get('sbbol.session.' . $this->product . '.' . $user_id);
    }

    /**
     * @param $client
     */
    public function revokeAccessToken(SBBOLTokens $client): void
    {
        $client->expires_in = 0;
        $client->save();
    }
}
