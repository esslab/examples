<?php

namespace App\Services\Smartpay;

use App\Exceptions\CRMException;

use App\Http\Requests\V2\Smartpay\DeletePaymentRequest;
use App\Services\CRM\CRMService;
use App\Models\Contract;
use Illuminate\Support\Collection;

class PaymentService extends CRMService
{
    /**
     * INT7.100 Запрос графика платежей
     * получение списка недирективных платежей
     * @param Contract $contract
     * @return Collection
     * @throws CRMException
     */
    public function getScheduleList(Contract $contract)
    {
        $ret = $this->Call('GetPaymentSchedule', [
            'ID' => $contract->sid,
            'ContractId' => $contract->sid,
        ], false);

        $data = collect();

        foreach ($ret['ListOfPaymentSchedule']->PaymentSchedule ?? [] as $item) {
            $o = new \stdClass();
            $o->PaymentDate = $item->DateOfPayment;
            $data->push($o);
        }

        return $data;
    }

    /**
     * INT7.101 Удаление директивных платежей
     * @param Contract $contract
     * @param string $templateId
     * @param string $paymentDate
     * @return mixed
     * @throws CRMException
     */
    public function destroy(Contract $contract, string $templateId, string $paymentDate)
    {
        return $this->Call('InitiateDirectPayment', [
            'PaymentText' => 'Deleted payment',
            'ActionType' => 'Delete',
            'TemplateId' => $templateId,
            'PaymentAmount' => 0,
            'PaymentDate' => $paymentDate,
            'languageID' => 'RUS',
            'ContractId' => $contract->sid,
        ]);
    }

    /**
     * INT7.101 Инициирование директивного платежа
     * создание и удаление директивных платежей
     * @param Contract $contract
     * @param $data
     * @return mixed
     * @throws CRMException
     */
    public function createDirective(Contract $contract, $data)
    {
        $result = $this->Call('InitiateDirectPayment', [
            'PaymentText' => $data->PaymentText,
            'ActionType' => 'Add',
            'PaymentAmount' => $data->PaymentAmount,
            'PaymentDate' => $data->PaymentDate,
            'languageID' => 'RUS',
            'ContractId' => $contract->sid,
        ]);

        return $result;
    }
}