<?php

namespace App\Http\Controllers\V2\Smartpay;

use App\Models\Smartpay\CashbackMetrics;
use App\Events\EmailPhoneFilled;
use App\Exceptions\ApiException;
use App\Exceptions\CRMException;
use App\Http\Controllers\V2\Controller;
use Carbon\Carbon;
use App\Http\Requests\V2\Smartpay\{CheckPaymentAvailableRequest, GenerateUrlRequest, SavePaymentResultRequest};
use App\Models\Contract;
use App\Services\CRM\Smartpay\SmartpayService;
use App\Services\Smartpay\AcquiringService;
use App\Models\CRM\Smartpay\{CheckPaymentAvail, GpbCheckPayment, GpbPaymentLimits, GpbSavePayment, SavePaymentResult};
use App\Resources\GPB\GPBUrlGenerator;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Cache;

class AcquiringController extends Controller
{
    public const OVERLIMIT_MESSAGE = 'Сумма превышает оставшийся лимит';
    public const CONTRACT_DOESNT_BELONG = 'Указанный договор не принадлежит данному контрагенту. Пожалуйста, перезагрузите страницу или авторизуйтесь повторно.';

    /**
     * @param Contract $contract
     * @return ResponseFactory|Response
     */
    public function checkGpbLimits(Contract $contract)
    {
        return $this->response($this->getGpbLimits($contract->sid));
    }

    /**
     * @param GenerateUrlRequest $request
     * @param Contract $contract
     * @return ResponseFactory|Response
     * @throws \Exception
     */
    public function generateUrl(GenerateUrlRequest $request, Contract $contract)
    {
        if (self::isLimitEnabled() && !$this->isAvailableToPay($contract->sid, $request->amount)) {
            throw new ApiException(self::OVERLIMIT_MESSAGE);
        }

        if (!$this->doesContractBelong($contract)) {
            throw new ApiException(self::CONTRACT_DOESNT_BELONG);
        }

        $mobile = $request->header('x-mobile-client', false);
        $generator = new GPBUrlGenerator($request->card_type, $mobile);
        $generator->fillFromRequest($request, $contract);

        event(new EmailPhoneFilled($this->getUser(), EmailPhoneFilled::CHANNEL_SMARTPAY, $request->email, ''));

        return $this->response([
            'targetUrl' => $generator->getUrl(),
        ]);
    }

    /**
     * @param CheckPaymentAvailableRequest $request
     * @param CheckPaymentAvail $crmPayment
     * @param GpbCheckPayment $gpbPayment
     * @return mixed
     */
    public function check(
        CheckPaymentAvailableRequest $request,
        CheckPaymentAvail $crmPayment,
        GpbCheckPayment $gpbPayment
    ) {
        return AcquiringService::checkOrSave($request, $crmPayment, $gpbPayment);
    }

    /**
     * @param SavePaymentResultRequest $request
     * @param SavePaymentResult $crmPayment
     * @param GpbSavePayment $gpbPayment
     * @return mixed
     */
    public function pay(
        SavePaymentResultRequest $request,
        SavePaymentResult $crmPayment,
        GpbSavePayment $gpbPayment
    ) {
        return AcquiringService::checkOrSave($request, $crmPayment, $gpbPayment);
    }

    /**
     * @param SmartpayService $smartpayService
     * @return ResponseFactory|Response
     * @throws CRMException
     */
    public function getBankCards(SmartpayService $smartpayService)
    {
        $cards = $smartpayService->getBankCards(auth()->user());
        $cards->transform(function ($item) {
            return [
                'CardID' => $item->id,
                'MaskedPan' => $item->number,
            ];
        });

        return $this->response(['bcards_list' => $cards]);
    }

    /**
     * @param string $bankCardId
     * @param Contract $contract
     * @param SmartpayService $smartpayService
     * @return ResponseFactory|Response
     * @throws CRMException
     */
    public function deleteBankCard(Contract $contract, string $bankCardId, SmartpayService $smartpayService)
    {
        $smartpayService->deleteBankCard(auth()->user(), $bankCardId);

        return $this->response('', Response::HTTP_OK);
    }

    /**
     * @return bool
     */
    public static function isLimitEnabled(): bool
    {
        return config('smartpay.providers.acquiring.limit_enabled');
    }

    /**
     * @param string $contract_id
     * @return array
     */
    private function getGpbLimits(string $contract_id): array
    {
        $todayPayments = GpbPaymentLimits::where('contract_id', $contract_id)->first();

        $paidToday = 0;
        $availableSum = $limit = $todayPayments->limit ?? (int)config('smartpay.providers.acquiring.daily_limit');

        if ($todayPayments && $todayPayments->last_payment == date('Y-m-d')) {
            $paidToday = (double)($todayPayments->total_paid_sum ?? 0);
            $availableSum = $limit - $paidToday;
        }

        return [
            'is_limit_enabled' => self::isLimitEnabled(),
            'paid_today' => $paidToday,
            'available_sum' => $availableSum,
            'max_sum' => $limit,
        ];
    }

    /**
     * @param string $contract_id
     * @param float $amount
     * @return bool
     */
    public function isAvailableToPay(string $contract_id, float $amount): bool
    {
        $limits = $this->getGpbLimits($contract_id);

        return $amount <= $limits['available_sum'];
    }

    /**
     * @return ResponseFactory|Response
     */
    public function getCashbackTarget()
    {
        if (!empty($this->getUser()->client_id)) {
            $metrics = CashbackMetrics::where('client_id', $this->getUser()->client_id)
                ->whereDate('started_at', '<=', Carbon::today())
                ->whereDate('expired_at', '>=', Carbon::today())->first();

            return $this->response($metrics);
        }

        return $this->response([]);
    }

    /**
     * @param string $trx_id
     * @return ResponseFactory|Response
     */
    public function getPaymentStatus(string $trx_id)
    {
        if ($message = Cache::pull('gpb_payment_' . sha1($trx_id))) {
            return $this->response([
                'success' => false,
                'message' => $message,
            ]);
        }

        return $this->response(['success' => true]);
    }

    /**
     * @param Contract $contract
     * @return bool
     */
    private function doesContractBelong(Contract $contract): bool
    {
        return auth()->user()->contracts->contains('sid', $contract->sid);
    }
}
