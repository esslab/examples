<?php

namespace App\Http\Controllers\V2\Smartpay;

use App\Exceptions\SberAuthException;
use App\Clients\Smartpay\NeoClient;
use App\Http\Controllers\V2\Controller;
use App\Http\Requests\V2\Smartpay\CorcheckoutRequest;
use App\Http\Requests\V2\Smartpay\CreateCreditRequest;
use App\Http\Requests\V2\Smartpay\CreatePaymentRequest;
use App\Http\Requests\V2\Smartpay\SBBOLRequest;
use App\Models\Contract;
use App\Models\CRM\Smartpay\{AccountPayment,
    SberAccountPayment,
    SberCorcheckoutAccountPayment,
    SberCreditAccountPayment};
use App\Models\Smartpay\{AccountPaymentModel, SBBOLOauthAccessTokens};
use App\Notifications\ErrorHasOccurredNotification;
use Illuminate\Http\Response;
use App\Services\Smartpay\{SberbankAuthService, SberbankFintechService, SberbankService};
use App\Services\CRM\Smartpay\SmartpayService;
use Illuminate\Http\Request;
use Log;

class SberbankCreditController extends Controller
{
    private const NOT_SBBOL_CLIENT_URL = 'https://www.sberbank.ru/ru/s_m_business/open-accounts';
    private const INSTALLMENT_OOO = 'MB-K-oo-226';
    private const INSTALLMENT_IP = 'MB-K-ip-225';
    const NEW_VKL = 'New VKL';
    const CREDIT = 'Credit';
    private string $product;

    public function __construct()
    {
        if(request()->product == SberbankService::CORCHECKOUT) {
            $this->product=SberbankService::CORCHECKOUT;
        }

        if(request()->product == SberbankService::KVK) {
            $this->product=SberbankService::KVK;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function codeToToken(SBBOLRequest $request)
    {
        $redirect_url = $this->getRedirectUrl($request->just_auth);

        try {
            $sberbankService = new SberbankAuthService($request->product, null,false);
            [$user_id, $inn, $session] = $sberbankService->restoreAuthData($request->nonce);

            if ($this->isCorcheckout()) {
                $this->addParamsToRequest($request, $inn);
            }

            $token_response = $sberbankService->getTokens($request->code);
            $this->log('Requesting user-info for inn check');
            $user_info = $sberbankService->getUserInfo();

            if ($this->innIsMatch($user_info['inn'], $inn)) {
                $this->log('INN ' . $user_info['inn'] . ' does not match');

                return redirect($redirect_url . '?error=inn');
            }

            $sberbankService->saveTokens($token_response, $user_id, $session);
        } catch (\Exception $e) {
            $this->log($e->getMessage());

            return redirect($redirect_url . '?error=login');
        }

        try {
            if ($this->isCorcheckout() && $request->contract) {
                return  redirect($this->getCorcheckoutRedirectUrl($this->pay($request)));
            }
        } catch (\Exception $e) {
            Log::channel($request->product)->error('Failed payment: ' . $e->getMessage());

            return redirect($this->getLkUrl() . SberAccountPayment::CORCHECKOUT_ERROR);
        }

        return redirect($redirect_url);
    }

    /**
     * @param SBBOLRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function authorize(SBBOLRequest $request)
    {
        return $this->response([
            'redirect_url' => SberbankAuthService::getAuthorizationLink($request->product)
        ]);
    }

    /**
     * @param SberbankAuthService $SBBOLService $SBBOLService
     * @param SberbankFintechService $SBBOLFintechService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function userInfo(SberbankAuthService $SBBOLService)
    {
        $this->product = SberbankFintechService::KVK;
        $user = $SBBOLService->getUserInfo();
        $this->saveCryptoType($user);

        $offers_user = $offers_gpn = [];

        if (!empty($user['buyOnCreditMmb']) && !empty($user['orgLawFormShort'])) {
            try {
                $this->log('Requesting offers');
                $SBBOLFintechService = new SberbankFintechService();
                $offers_user = $SBBOLFintechService->getCreditOffers($user['orgLawFormShort']);
                $SBBOLFintechService->setServiceToken();
                $this->log('Requesting offers for GPN');
                $offers_gpn = $SBBOLFintechService->getCreditOffers($user['orgLawFormShort']);
            } catch (\Exception $e) {
                $this->log('Getting credit offers: ' . $e->getMessage());

                throw $e;
            }
        }

        $offers_gpn = $this->mergeOffers($offers_user, $offers_gpn);

        $response = [
            'offer_smart_credit' => $user['offerSmartCredit'] ?? null,
            'buy_on_credit_mmb' => $user['buyOnCreditMmb'] ?? null,
            'sum_offer_smart_credit' => $user['summOfferSmartCredit'] ?? null,
            'inn' => $user['inn'] ?? null,
            'org_law_form_short' => $user['orgLawFormShort'] ?? null,
            'has_active_credit_line' => $user['hasActiveCreditLine'] ?? null,
            'credit_line_available_sum' => $user['creditLineAvailableSum'] ?? null,
            'offers' => !empty($offers_gpn) ? $offers_gpn->flatten(1) : $offers_gpn,
        ];

        $this->log('User-info response: ' . json_encode($response));

        return $this->response($response);
    }

    /**
     * @param SmartpayService $smartpayService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getCorcheckoutOutstandingPayments(SmartpayService $smartpayService)
    {
        $payments = $smartpayService->getPendingPayments([
            'client_id'=> auth()->user()->client_info()->id,
            'partner' => SberAccountPayment::PARTNER,
            'product_type' => SberCorcheckoutAccountPayment::PRODUCT_TYPE[0]
        ]);

        try {
            if ($payments) {
                new SberbankFintechService(SberbankService::CORCHECKOUT);
            };
        } catch (SberAuthException $e) {
            return $this->response([
                'error' => Response::HTTP_LOCKED,
                'message' => 'Tokens have not exist or expired'
            ],
                Response::HTTP_LOCKED
            );
        }


        return $this->response($payments);
    }

    /**
     * @param SmartpayService $smartpayService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getOutstandingPayments(SmartpayService $smartpayService)
    {
        $payments = $smartpayService->getPendingPayments([
                'client_id'=> auth()->user()->client_info()->id,
                'partner' => SberAccountPayment::PARTNER,
            ]);

        $payments = array_filter($payments, function($payment) {
            return $payment['PaymentProductType'] <> SberCorcheckoutAccountPayment::PRODUCT_TYPE[0];
        });

        return $this->response($payments);
    }

    /**
     * @param Contract $contract
     * @param CreatePaymentRequest $request
     * @param SmartpayService $smartpayService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\CRMException
     */
    public function payByBalance(
        Contract $contract,
        CreatePaymentRequest $request,
        SmartpayService $smartpayService
    ) {
        $payment = new SberCreditAccountPayment($request, $contract);
        $payment = $this->savePayment($smartpayService, $payment, $request->email);
        $sberbankService = new SberbankFintechService(SberbankService::KVK, $request->user_id ?? null);
        $payment->product = SberCreditAccountPayment::PRODUCT;
        $sberbankService->formInvoiceAny($payment, $contract, $request->credit_contract_number);

        $crypto = SBBOLOauthAccessTokens::where('user_id', auth()->user()->id)->first();

        if ($crypto->crypto_type === $payment::CRYPTO_TOKEN) {
            return $this->response([
                'redirect_url' => config('smartpay.providers.sbbol.token_auth') . $payment::CREDIT_REQUEST_URL . $payment->id_transaction
            ]);
        }

        return $this->response([
            'redirect_url' => config('smartpay.providers.sbbol.sso_domain') . '/ic/dcb/index.html#/payment-creator/' . $payment->id_transaction . '?backUrl=' . config('api.lk_url') . SberAccountPayment::CREDIT_OFFER_PAGE
        ]);
    }

    /**
     * @param Contract $contract
     * @param Request $request
     * @param SmartpayService $smartpayService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \App\Exceptions\CRMException
     */
    public function payByCorcheckout(
        Contract $contract,
        CorcheckoutRequest $request,
        SmartpayService $smartpayService
    ) {
        $request->validate([
            'amount' => 'required|numeric',
            'email' => 'sometimes|email',
        ]);

        try {
            $sberService = new SberbankFintechService(SberbankService::CORCHECKOUT);
        } catch (SberAuthException $e) {
            return $this->response([
                'redirect_url' => SberbankAuthService::getAuthorizationLink(SberbankService::CORCHECKOUT, [
                    'contract' => $contract->sid,
                    'email' => $request->email,
                    'amount' =>  $request->amount,
                    'type' =>  $request->type,
                ])
            ]);
        }

        $payment = $this->payByInvoice($request, $contract, $smartpayService, $sberService);

        return  $this->response([
            'redirect_url' => $this->getCorcheckoutRedirectUrl($payment)
        ]);
    }

    /**
     * @param Contract $contract
     * @param CreateCreditRequest $request
     * @param SmartpayService $smartpayService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function createCreditRequest(
        Contract $contract,
        CreateCreditRequest $request,
        SmartpayService $smartpayService
    ) {
        if ($request->product_type == self::NEW_VKL || $request->product_type == self::CREDIT) {
            $this->hitTheLead(config('smartpay.providers.sbbol.is_client_hash'));
        }

        $payment = new SberCreditAccountPayment($request, $contract);
        $sberbankService = new SberbankFintechService($payment::PRODUCT);
        $payment = $this->savePayment($smartpayService, $payment, $request->email);

        try {
            $sberbankService->createCreditRequest($payment, $contract);
        } catch (\Exception $e) {
            $payment->crm_status = AccountPayment::CANCEL;
            $smartpayService->saveAccountPayment($payment);
            Log::channel('smartpay_log')->error('Payment ' . $payment->trx_id . ' was aborted');
            throw $e;
        }

        $crypto = SBBOLOauthAccessTokens::where('user_id', auth()->user()->id)->first();

        if ($crypto->crypto_type === $payment::CRYPTO_TOKEN) {
            return $this->response([
                'redirect_url' => config('smartpay.providers.sbbol.token_auth') . $payment::CREDIT_REQUEST_URL . $payment->id_transaction
            ]);
        }

        return $this->response([
            'redirect_url' => config('smartpay.providers.sbbol.sso_domain') . $payment::CREDIT_REQUEST_URL . $payment->id_transaction
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getNotClientLink()
    {
        $this->hitTheLead(config('smartpay.providers.sbbol.not_client_hash'));

        return $this->response([
            'redirect_url' => self::NOT_SBBOL_CLIENT_URL
        ]);
    }

    /**
     * @param $user
     */
    private function saveCryptoType($user): void
    {
        $access_client = SBBOLOauthAccessTokens::where('user_id', auth()->user()->id)->first();
        $access_client->crypto_type = $user['userCryptoType'];
        $access_client->save();
    }

    /**
     * @param SmartpayService $smartpayService
     * @param $payment
     * @param string $email
     * @return SberAccountPayment
     * @throws \App\Exceptions\CRMException
     */
    private function savePayment(SmartpayService $smartpayService, $payment, $email = ''): SberAccountPayment
    {
        $payment->trx_id = $payment->id_transaction = $smartpayService->verifyAccountPayment($payment->contract, $payment->amount, $email);
        $smartpayService->saveAccountPayment($payment);

        try {
            AccountPaymentModel::create([
                'contract'       => $payment->contract,
                'id_transaction' => $payment->id_transaction,
                'partner'        => $payment->partner,
                'product'        => $payment->product,
                'login'          => $payment->login,
                'user_id'        => auth()->user()->id ?? $payment->user_id,
                'status'         => $payment->getStatus(),
            ]);
        } catch(\Exception $e) {
            Log::channel($payment->product)->info('Не сохранилась запись в таблицу account_payment ' . $payment->id_transaction);
        }

        return $payment;
    }

    /**
     * @param array $offers_user
     * @param array $offers_gpn
     * @return array|\Illuminate\Support\Collection
     */
    private function mergeOffers(array $offers_user, array $offers_gpn)
    {
        if ($offers_user) {
            $offers_user = collect($offers_user)->groupBy('productCode');
            $offers_gpn = collect($offers_gpn)->groupBy('productCode');

            $product = null;

            if (!empty($offers_user[self::INSTALLMENT_OOO])) {
                $product = self::INSTALLMENT_OOO;
            } else if (!empty($offers_user[self::INSTALLMENT_IP])) {
                $product = self::INSTALLMENT_IP;
            }

            if ($product) {
                if (empty($offers_user[$product])) {
                    abort(200, 'Нет предложений');
                }

                $offers['dateSince'] = $offers_user[$product]->get(0)['dateSince'];
                $offers['dateUntil'] = $offers_user[$product]->get(0)['dateUntil'];
                $offers['availableSum'] = $offers_user[$product]->get(0)['availableSum'];
                $offers['contractNumber'] = $offers_user[$product]->get(0)['contractNumber'];
                $offers = array_merge($offers_user[$product]->get(0), $offers);

                if (!empty($offers_gpn[$product])) {
                    $offers_gpn[$product]->put('0', $offers);
                }
            }
        }

        return $offers_gpn;
    }

    /**
     * @param string $hash
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function hitTheLead(string $hash)
    {
        if (config('smartpay.providers.sbbol.sber_hit_the_lead_is_enabled')) {
            $inn = auth()->user()->client_info()->inn;
            $client = new NeoClient(config('smartpay.providers.sbbol.hit_the_lead_domain'));
            try {
                $client->request('POST', '/api/v1/sber_mq/external_form/' . $hash .'/order?data[inn]=' . $inn);
            } catch (\Exception $e) {
                Log::channel('neo')->error('Не удалось отправить запрос на сохранение лида');

                if (!str_contains($e->getMessage(), 'blocked_by_me')) {
                    \Notification::route('mail', explode(',', config('logging.emails.smartpay')))
                        ->notify(new ErrorHasOccurredNotification($e, 'Кредит в корзине'));
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getLkUrl(): string
    {
        return config('smartpay.providers.sbbol.enabled') ? config('api.lk_url') : config('smartpay.lk_test_url');
    }

    /**
     * @param Request $request
     * @param Contract $contract
     * @param SmartpayService $smartpayService
     * @param SberbankFintechService $sberService
     * @return SberAccountPayment
     * @throws \App\Exceptions\CRMException
     */
    private function payByInvoice(Request $request, Contract $contract, SmartpayService $smartpayService, SberbankFintechService $sberService): SberAccountPayment
    {
        $payment = new SberCorcheckoutAccountPayment($request, $contract);
        $payment->login = auth()->user()->login ?? $request->login;
        $payment->inn = $request->inn;
        $payment->user_id = $request->user_id;
        $payment = $this->savePayment($smartpayService, $payment, $request->email);
        $sberService->formInvoice($payment, $contract);

        return $payment;
    }

    /**
     * @param string $product
     * @param bool $payment_page
     * @return string
     */
    private function getRedirectUrl($payment_page = false): string
    {
        if ($this->product === SberbankService::CORCHECKOUT) {
            return $this->getLkUrl() . ($payment_page ? SberAccountPayment::CORCHECKOUT_SUCCESS : SberAccountPayment::PAYMENT_PAGE);
        } elseif ($this->product === SberbankService::KVK) {
            return $this->getLkUrl() . SberAccountPayment::CREDIT_OFFER_PAGE;
        }

        return abort(Response::HTTP_CONFLICT, 'Product required');
    }

    /**
     * @param SberAccountPayment $payment
     * @return string
     */
    private function getCorcheckoutRedirectUrl(SberAccountPayment $payment): string
    {
        return config('smartpay.providers.sbbol.sso_domain')
            . '/ic/dcb/index.html#/payment-creator/' . $payment->id_transaction
            . '?backUrl=' . config('api.lk_url') . SberAccountPayment::CORCHECKOUT_SUCCESS
            . '&negativeUrl=' . config('api.lk_url') . SberAccountPayment::CORCHECKOUT_ERROR;
    }

    /**
     * @param SBBOLRequest $request
     * @return SberAccountPayment
     * @throws \App\Exceptions\CRMException
     */
    private function pay(SBBOLRequest $request): SberAccountPayment
    {
        $contract = (new Contract())->where('sid', $request->contract)->first();
        $sberService = new SberbankFintechService(SberbankService::CORCHECKOUT, $request->user_id);

        return $this->payByInvoice($request, $contract, new SmartpayService($request->login), $sberService);
    }

    /**
     * @param SberbankAuthService $sberbankService
     * @param SBBOLRequest $request
     * @param $inn
     */
    private function addParamsToRequest(SBBOLRequest $request, $inn): void
    {
        $params = SberbankAuthService::restoreParams($request->nonce);
        $request->merge($params);
        $request->merge(['inn' => $inn]);
    }

    /**
     * @param string $user_info_inn
     * @param string $inn
     * @return bool
     */
    private function innIsMatch(string $user_info_inn, string $inn): bool
    {
        return $user_info_inn !== $inn && $user_info_inn !== config('smartpay.providers.sbbol.trusted_inn');
    }

    /**
     * @param string $message
     */
    private function log(string $message): void
    {
        Log::channel($this->product)->info($message);
    }

    /**
     * @param string $product
     * @return bool
     */
    public function isCorcheckout(): bool
    {
        return $this->product == SberbankService::CORCHECKOUT;
    }
}
