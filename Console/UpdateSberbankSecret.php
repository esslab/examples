<?php

namespace App\Console\Commands\Smartpay;

use App\Helpers\CipherHelper;
use App\Helpers\EmailReportHelper;
use App\Models\Smartpay\SBBOLOauthClients;
use App\Services\Smartpay\SberbankAuthService;
use App\Services\Smartpay\SberbankService;
use App\Support\GuidGenerator;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateSberbankSecret extends Command
{
    private const SECRET_EXPIRING_DAYS_ALERT = 30;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-secret {product} {days=20} {--user=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating Sberbank secret';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting update');

        $this->createClient();
        $client = SberbankService::getOauthClient($this->argument('product'));

        if ($client) {
            if (Carbon::now()->subDays($this->argument('days')) < $client->updated_at) {
                $this->info('Ключ был обновлен ранее. Последняя дата обнолвения ' . $client->updated_at);
                exit();
            }

            $this->reportIfExpired($client->updated_at);

            try {
                $this->updateSecret($client);
            } catch (\Exception $e) {
                $this->error($e->getMessage());
                abort($e->getCode(), $e->getMessage());
            }
        } else {
            $this->error('There is no client to update secret');
        }
    }

    /**
     *
     */
    private function createClient(): void
    {
        if ($user_id = $this->option('user')) {
            $this->info('Creating client for user ' . $user_id);
            $this->updateOauthClient($user_id);
        }
    }

    /**
     * @param $client
     */
    private function updateSecret($client): void
    {
        $this->info('Oauth client record ' . $client->user_id . ' found. Updating... '  . $client->secret);
        $sbbolService = new SberbankAuthService($this->argument('product'), $client->user_id, true);
        $secret = $sbbolService->decrypt($client, $client->secret ?? config('smartpay.providers.sbbol.'. $this->argument('product') .'.client_secret'));
        $new_secret = $this->generateSecret();
        $sbbolService->updateSecret($secret, $new_secret, $client->client_id);
        $this->updateOauthClient($client->user_id, $new_secret);
        $this->info('Secret was updated');
    }


    /**
     * @param $user_id
     * @param null $new_secret
     */
    private function updateOauthClient($user_id, $new_secret = null): void
    {
        $encrypted_secret = $this->prepareSecret($user_id, $new_secret);
        $this->updateClient($user_id, $encrypted_secret['secret'], $encrypted_secret['iv']);
        Log::channel($this->argument('product'))->info('Был обновлен ключ secret');
        EmailReportHelper::sendReport('Client secret был обновлен', 'smartpay');
        $this->info('Oauth client record ' . $user_id . ' was updated '  . $encrypted_secret['secret']);
    }

    /**
     * @param $user_id
     * @param null|string $new_secret
     * @return array
     */
    private function prepareSecret(string $user_id, ?string $new_secret = null): array
    {
        $iv = CipherHelper::generateIV();
        $secret = CipherHelper::safeEncrypt(
            $new_secret ?? config('smartpay.providers.sbbol.' . $this->argument('product') . '.client_secret'),
            SberbankService::getKey($user_id),
            $iv
        );

        return [
            'iv' => $iv,
            'secret' => $secret
        ];
    }

    /**
     * @param $user_id
     * @param $encrypted_secret
     * @param $iv
     */
    private function updateClient($user_id, $encrypted_secret, $iv): void
    {
        $oauthClients = new SBBOLOauthClients();
        $oauthClients->updateOrCreate(
            ['user_id' => $user_id, 'name' => config('smartpay.providers.sbbol.' . $this->argument('product') . '.client_name')],
            [
                'user_id' => $user_id,
                'name' => config('smartpay.providers.sbbol.' . $this->argument('product') . '.client_name'),
                'client_id' => config('smartpay.providers.sbbol.' . $this->argument('product') . '.client_id'),
                'secret' => $encrypted_secret,
                'iv' => base64_encode($iv)
            ]
        );
    }

    /**
     * @return string
     */
    private function generateSecret(): string
    {
        return GuidGenerator::randomString();
    }

    /**
     * @param $date
     */
    private function reportIfExpired($date): void
    {
        if (Carbon::now()->subDays(self::SECRET_EXPIRING_DAYS_ALERT) > $date) {
            EmailReportHelper::sendReport(
                'Secret для sberbank не обновлялся более ' . self::SECRET_EXPIRING_DAYS_ALERT . ' дней',
                'smartpay'
            );
        }
    }
}
