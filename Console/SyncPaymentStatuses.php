<?php

namespace App\Console\Commands\Smartpay;

use App\Models\CRM\Smartpay\AccountPayment;
use App\Models\CRM\Smartpay\SberCorcheckoutAccountPayment;
use App\Models\CRM\Smartpay\SberCreditAccountPayment;
use App\Models\Smartpay\AccountPaymentModel;
use App\Models\Smartpay\SBBOLCorcheckAccessTokens;
use App\Models\Smartpay\SBBOLOauthAccessTokens;
use App\Services\CRM\CRMService;
use App\Services\Smartpay\{PayableService, SberbankFintechService, SberbankService, YandexKassaService, SbpService};
use App\Services\CRM\Smartpay\SmartpayService;
use Illuminate\Console\Command;

class SyncPaymentStatuses extends Command
{
    /**
     * Синхронизация статусов платежей Банк - CRM для конкретного партнера
     *
     * @var string
     */
    protected $signature = 'sync-payments {partner} {--force-cancel} {--cancel-by-step} {--product=}';
    protected CRMService $smartpayService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация статусов платежей Банк - CRM для конкретного партнера';
    private string $product;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->smartpayService = new SmartpayService();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting...');

        try {
            if (!in_array($this->argument('partner'), AccountPayment::AVAILABLE_PARTNERS)) {
                abort(400, 'Партнер не найден');
            }

            $this->info('Starting sync for ' . $this->argument('partner'));
            $payments = $this->smartpayService->getPendingPayments(['partner' => ucfirst($this->argument('partner'))]);
            $this->info('Total ' . collect($payments)->count() . ' payments found');
            $this->syncPayments($payments);
            $this->info('Sync has finished');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * @param
     */
    private function syncPayments($payments): void
    {
        $this->updatePaymentStatuses($payments);
    }

    /**
     * @param $payments
     */
    private function updatePaymentStatuses($payments): void
    {
        if (!$this->option('force-cancel') || ($this->option('force-cancel') && $this->confirm('Все платежи будут отменены. Продолжить?'))) {
            foreach ($payments as $payment) {
                try {
                    $this->info('Going to sync ' . $payment['IdTransaction']);
                    $service = $this->getService($payment);

                    if (!$this->option('cancel-by-step') || ($this->option('cancel-by-step') && $this->confirm('Платеж будет отменен. Продолжить?'))) {
                        $this->info('Syncing ' . $payment['IdTransaction']);
                        $payment = $service->updateStatuses($payment, $this->option('cancel-by-step') || $this->option('force-cancel'));
                        $this->info('Payment ' . json_encode($payment->id_transaction) . ' was updated');
                    } else {
                        $this->info('Payment was skipped');
                    }
                } catch (\Exception $e) {
                    $this->info($e->getMessage());
                }
                $this->info('-------------');
            }
        } else {
            $this->info('Выполнение команды отменено');
        }
    }

    /**
     * @param array $payment
     * @return YandexKassaService
     */
    private function getService(array $payment): PayableService
    {
        if ($this->argument('partner') === 'yandex') {
            return new YandexKassaService();
        }

        if ($this->argument('partner') === 'SBP') {
            return new SbpService();
        }

        if ($this->argument('partner') === 'sberbank') {
            $accountPayment = AccountPaymentModel::where('id_transaction', $payment['IdTransaction'])->first();

            if ($this->option('cancel-by-step') || $this->option('force-cancel')) {
                return new SberbankFintechService(SberbankService::KVK, null, false);
            }

            $user_id = '';

            if ($accountPayment) {
                $user_id = $accountPayment->user_id;
            } else {
                abort(400, 'Нет записи с номером транзакции ' . $payment['IdTransaction']);
            }

            if (in_array($payment['PaymentProductType'], SberCreditAccountPayment::PRODUCT_TYPE)) {
                $client = SBBOLOauthAccessTokens::where('user_id', $user_id)->first();
                $this->product = SberbankService::KVK;
            }

            if (in_array($payment['PaymentProductType'], SberCorcheckoutAccountPayment::PRODUCT_TYPE)) {
                $client = SBBOLCorcheckAccessTokens::where('user_id', $user_id)->first();
                $this->product = SberbankService::CORCHECKOUT;
            }

            if (!empty($client)) {
                $service = new SberbankFintechService($this->product, $user_id);
                $service->revokeAccessToken($client);
                $this->info('Token for user ' . $user_id . ' was set');

                return $service;
            }

            abort(400, 'Нет токена для пользователя ' . $user_id);
        }

        abort(400, 'Сервис партнера не найден');
    }
}
