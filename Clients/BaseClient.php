<?php

namespace App\Clients\Common;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Log;

abstract class BaseClient extends Client
{
    public const ACCEPT_JOSE = ['Accept' => 'application/jose'];
    public const CONTENT_JSON = ['Content-Type' => 'application/json'];
    public const CONTENT_URLENCODED = ['Content-Type' => 'application/x-www-form-urlencoded'];
    public const ACCEPT_JSON = ['Accept' => 'application/json'];
    public $headers = ['headers' => []];
    public string $channel = 'daily';
    public $throwable = true;
    protected $base_uri;
    public const NOT_REPORTABLE = [404];

    public function __construct(array $config = [])
    {
        $config = array_merge([
            'base_uri' => $this->base_uri,
            'proxy'    => config('services.proxy.dsn'),
            'verify' => false,
            'handler' => $this->createLoggingHandlerStack([
                'REQUEST: {method} {uri} HTTP/{version} {req_body}',
                'RESPONSE ({uri}): {code} - {res_body} - {error}',
            ]),
        ], $config);

        parent::__construct($config);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @return mixed|\Psr\Http\Message\ResponseInterface|string
     * @throws GuzzleException
     */
    public function request($method, $uri = '', array $options = [])
    {
        try {
            $response_object = parent::request($method, $uri, array_merge($this->headers, $options));

            return (string) $response_object->getBody();
        } catch (GuzzleException $e) {
            if ($this->throwable) {
                if ($this->isNotReportable($e->getCode())) {
                    throw $e;
                }

                report($e);
            }
        }
    }

    /**
     * @param int $code
     * @return bool
     */
    public function isNotReportable(int $code)
    {
        return in_array($code, static::NOT_REPORTABLE);
    }

    /**
     * @return mixed
     */
    protected function getLogger() {
        return Log::channel($this->channel);
    }

    /**
     * @param array $messageFormats
     * @return HandlerStack
     */
    private function createLoggingHandlerStack(array $messageFormats): HandlerStack
    {
        $stack = HandlerStack::create();

        collect($messageFormats)->each(function ($messageFormat) use ($stack) {
            $stack->unshift(
                $this->createGuzzleLoggingMiddleware($messageFormat)
            );
        });

        return $stack;
    }

    /**
     * @param string $messageFormat
     * @return callable
     */
    private function createGuzzleLoggingMiddleware(string $messageFormat): callable
    {
        return Middleware::log(
            $this->getLogger(),
            new MessageFormatter($messageFormat)
        );
    }

    /**
     * @param $header
     */
    public function setHeader(array $header): void
    {
        $this->headers = [
            'headers' => array_merge($this->headers['headers'], $header)
        ];
    }

    public function clearHeader(string $name)
    {
        unset($this->headers['headers'][$name]);
    }

    /**
     * @param string $access_token
     */
    public function setAuthHeader(string $access_token): void
    {
        $header = ['Authorization' => 'Bearer ' . $access_token];
        $this->setHeader($header);
    }

    public function setBasicAuthHeader(string $login, string $password): void
    {
        $header = ['Authorization' => 'Basic ' . base64_encode($login . ':' . $password)];
        $this->setHeader($header);
    }

    /**
     * @param array $params
     * @return string
     */
    public function getQueryParamsWithNoIndexes(array $params): string
    {
        return preg_replace('/(%5B)\d+(%5D)/i', '$1$2', http_build_query($params));
    }
}
