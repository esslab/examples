<?php
namespace App\Clients\Smartpay;

use App\Clients\Common\BaseClient;

class NeoClient extends BaseClient
{
    public string $channel = 'neo';
    public const NOT_REPORTABLE = [404, 422];

    /**
     * SberbankClient constructor.
     * @param string $base_uri
     */
    public function __construct(string $base_uri)
    {
        $this->base_uri = $base_uri;
        parent::__construct();
    }
}
