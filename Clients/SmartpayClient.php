<?php

namespace App\Clients\Smartpay;

use App\Clients\Common\BaseClient;

class SmartpayClient extends BaseClient
{
    public string $channel = 'smartpay_log';

    public function __construct(array $config = [])
    {
        $this->base_uri = config('smartpay.providers.' . $this->channel . '.api_url');

        parent::__construct($config);
    }
}
