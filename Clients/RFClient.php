<?php
namespace App\Clients\Smartpay;

use App\Clients\Common\BaseClient;

class RFClient extends BaseClient
{
    public string $channel = 'sbp';

    public function __construct(array $config = [])
    {
        $this->headers['headers'] = self::CONTENT_JSON;

        parent::__construct($config);
    }
}
