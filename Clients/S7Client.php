<?php

namespace App\Clients\Smartpay;

use App\Clients\Common\BaseClient;

class S7Client extends BaseClient
{
    public string $channel = 's7';
}
